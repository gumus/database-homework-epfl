/*
 Navicat Premium Data Transfer

 Source Server         : localConnection
 Source Server Type    : MySQL
 Source Server Version : 50623
 Source Host           : localhost
 Source Database       : Homework01

 Target Server Type    : MySQL
 Target Server Version : 50623
 File Encoding         : utf-8

 Date: 03/22/2015 17:31:55 PM
*/

USE MovieBase;
SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `Alternative_name`
-- ----------------------------
DROP TABLE IF EXISTS `Alternative_name`;
CREATE TABLE `Alternative_name` (
  `id` int(11) NOT NULL DEFAULT '0',
  `person_id` int(11) NOT NULL,
  `name` char(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`person_id`),
  KEY `person_id` (`person_id`),
  CONSTRAINT `alternative_name_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `Person` (`id`) ON DELETE CASCADE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Alternative_title`
-- ----------------------------
DROP TABLE IF EXISTS `Alternative_title`;
CREATE TABLE `Alternative_title` (
  `id` int(11) NOT NULL DEFAULT '0',
  `production_id` int(11) NOT NULL,
  `title` char(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`production_id`),
  KEY `production_id` (`production_id`),
  CONSTRAINT `alternative_title_ibfk_1` FOREIGN KEY (`production_id`) REFERENCES `Production` (`id`) ON DELETE CASCADE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Characters`
-- ----------------------------
DROP TABLE IF EXISTS `Characters`;
CREATE TABLE `Characters` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` TEXT(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Company`
-- ----------------------------
DROP TABLE IF EXISTS `Company`;
CREATE TABLE `Company` (
  `id` int(15) NOT NULL DEFAULT '0',
  `country_code` char(10) DEFAULT NULL,
  `name` char(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Person`
-- ----------------------------
DROP TABLE IF EXISTS `Person`;
CREATE TABLE `Person` (
  `id` int(10) NOT NULL DEFAULT '0',
  `name` char(255) DEFAULT NULL,
  `gender` char(15) DEFAULT NULL,
  `trivia` TEXT(2000) DEFAULT NULL,
  `quotes` TEXT(2000) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `deathdate` date DEFAULT NULL,
  `birthname` char(255) DEFAULT NULL,
  `minibiography` TEXT(2000) DEFAULT NULL,
  `spouse` char(255) DEFAULT NULL,
  `height` float(6, 3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Production`
-- ----------------------------
DROP TABLE IF EXISTS `Production`;
CREATE TABLE `Production` (
  `id` int(11) NOT NULL DEFAULT '0',
  `genre` char(50) DEFAULT NULL,
  `title` TEXT(500) DEFAULT NULL,
  `production_year` int(11) DEFAULT NULL,
  `series_id` int(11) DEFAULT NULL,
  `season_number` int(11) DEFAULT NULL,
  `episode_number` int(11) DEFAULT NULL,
  `series_years` char(30) DEFAULT NULL,
  `kind` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Production_cast`
-- ----------------------------
DROP TABLE IF EXISTS `Production_cast`;
CREATE TABLE `Production_cast` (
  `cast_id` int(11) NOT NULL AUTO_INCREMENT,
  `production_id` int(11) NOT NULL DEFAULT '0',
  `person_id` int(11) NOT NULL DEFAULT '0',
  `character_id` int(11) DEFAULT '0',
  `role_name` char(50) NOT NULL,
  PRIMARY KEY (`cast_id`),
  KEY `production_id` (`production_id`),
  KEY `character_id` (`character_id`),
  CONSTRAINT `production_cast_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `Person` (`id`),
  CONSTRAINT `production_cast_ibfk_2` FOREIGN KEY (`production_id`) REFERENCES `Production` (`id`),
  CONSTRAINT `production_cast_ibfk_3` FOREIGN KEY (`character_id`) REFERENCES `Characters` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `Production_company`
-- ----------------------------
DROP TABLE IF EXISTS `Production_company`;
CREATE TABLE `Production_company` (
  `id` int(11) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `production_id` int(11) NOT NULL DEFAULT '0',
  `kind` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`,`production_id`,`company_id`),
  KEY `production_id` (`production_id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `production_company_ibfk_1` FOREIGN KEY (`production_id`) REFERENCES `Production` (`id`),
  CONSTRAINT `production_company_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `Company` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;

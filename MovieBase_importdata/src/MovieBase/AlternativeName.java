package MovieBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AlternativeName implements Insertable{
	
	private int id;
    private int person_id;
    private String name;
    
    public AlternativeName(String[] parts){

        this.id = Integer.parseInt(parts[0]);
        this.person_id = Integer.parseInt(parts[1]);
        this.name =  parts[2];

    }

    public void insert(Connection conn) throws SQLException{

        String query = "INSERT INTO Alternative_name (id, person_id, name) VALUES (?, ?, ?)";

        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.setInt (1, id);
        preparedStmt.setInt (2, person_id);
        preparedStmt.setString(3, name);
        preparedStmt.execute();
    }
    
    public String toString()
    {
        return this.id + "\t" + this.person_id + "\t" + this.name;

    }
}

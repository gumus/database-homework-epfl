package MovieBase;

import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CreditsGUI {

	private JFrame frmCredits;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreditsGUI window = new CreditsGUI();
					window.frmCredits.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CreditsGUI() 
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frmCredits = new JFrame();
		frmCredits.setResizable(false);
		frmCredits.setTitle("Credits");
		frmCredits.setVisible(true);
		frmCredits.setBounds(100, 100, 485, 689);
		frmCredits.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCredits.getContentPane().setLayout(null);
		frmCredits.setLocationRelativeTo(null);
		
		JLabel lblNewLabel = new JLabel(new ImageIcon("images/epfl_logo.jpg"));
		lblNewLabel.setBounds(105, 29, 264, 126);
		frmCredits.getContentPane().add(lblNewLabel);
		
		JLabel lblThisProgramIs = new JLabel("This program is written by:");
		lblThisProgramIs.setBounds(10, 186, 456, 32);
		frmCredits.getContentPane().add(lblThisProgramIs);
		
		JLabel lblKvanKamayOrun = new JLabel("K\u0131van\u00E7 Kamay (253799)");
		lblKvanKamayOrun.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblKvanKamayOrun.setBounds(10, 224, 456, 43);
		frmCredits.getContentPane().add(lblKvanKamayOrun);
		
		JLabel lblForThe = new JLabel("Or\u00E7un G\u00FCm\u00FC\u015F (228199)");
		lblForThe.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblForThe.setHorizontalAlignment(SwingConstants.CENTER);
		lblForThe.setBounds(10, 269, 456, 22);
		frmCredits.getContentPane().add(lblForThe);
		
		JLabel lblCurtisBezault = new JLabel("Curtis Bezault (253078)");
		lblCurtisBezault.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblCurtisBezault.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCurtisBezault.setBounds(10, 304, 456, 22);
		frmCredits.getContentPane().add(lblCurtisBezault);
		
		JLabel lblForThentroduction = new JLabel("<html>for the \"Introduction to Database Systems\" (CS 322) course in \u00C9cole<br>" + "Polytechnique F\u00E9d\u00E9rale de Lausanne (EPFL).");
		lblForThentroduction.setVerticalAlignment(SwingConstants.TOP);
		lblForThentroduction.setBounds(10, 332, 456, 43);
		frmCredits.getContentPane().add(lblForThentroduction);
		
		JLabel lblTheProfessor = new JLabel("The Instructor: Dr Satya Valluri");
		lblTheProfessor.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblTheProfessor.setBounds(10, 381, 456, 43);
		frmCredits.getContentPane().add(lblTheProfessor);
		
		JLabel label = new JLabel("<html>The Asistants:");
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label.setBounds(10, 430, 456, 16);
		frmCredits.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("<html>Danica Porobic<br>" + "Manos Karpathiotakis<br>" + "Matt Olma<br>" + "Iraklis Psaroudakis<br>" +
				"Mirjana Pavlovic<br>" + "Eleni Tzirita Zacharatou<br>");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_1.setBounds(99, 427, 367, 103);
		frmCredits.getContentPane().add(label_1);
		
		JLabel lblAllRightsReserved = new JLabel("All rights reserved. TM & Copyright 2015 ");
		lblAllRightsReserved.setHorizontalAlignment(SwingConstants.CENTER);
		lblAllRightsReserved.setBounds(10, 551, 456, 32);
		frmCredits.getContentPane().add(lblAllRightsReserved);
		
		JButton btnNewButton = new JButton("Return");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				frmCredits.setVisible(false);
				MovieBaseGUI mbGUI = new MovieBaseGUI();
			}
		});
		btnNewButton.setBounds(126, 591, 200, 50);
		frmCredits.getContentPane().add(btnNewButton);	
	}
}

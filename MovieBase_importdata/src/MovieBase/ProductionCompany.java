package MovieBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ProductionCompany implements Insertable
{
    private int id;
    private int production_id;
    private int company_id;
    private String kind;
    
    public ProductionCompany(String[] parts)
    {
        this.id = Integer.parseInt(parts[0]);
        this.production_id = Integer.parseInt(parts[1]);
        this.company_id = Integer.parseInt(parts[2]);
        this.kind = parts[3];
    }

    public void insert(Connection conn) throws SQLException{

        String query = "INSERT INTO Production_company (id, production_id, company_id, kind) VALUES (?, ?, ?, ?)";

        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.setInt (1, id);
        preparedStmt.setInt (2, production_id);
        preparedStmt.setInt (3, company_id);
        preparedStmt.setString(4, kind);
        preparedStmt.execute();
    }

    public String toString()
    {
        return id + "\t" + production_id + "\t" + company_id + "\t" + kind;
    }
}

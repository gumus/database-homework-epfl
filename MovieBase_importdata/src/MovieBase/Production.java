package MovieBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

public class Production implements Insertable{
	
	private int id;
	private String genre;
	private String title;
	private int production_year;
	private Integer series_id;
	private int season_number;
	private int episode_number;
	private String series_years;
	private String kind;
	
	public Production(String[] parts)
	{
		this.id = Integer.parseInt(parts[0]);
		this.genre = parts[1];
        this.title = parts[2];
		this.production_year = Integer.parseInt(parts[3]);
		if(parts[4].compareTo("") == 0)
			this.series_id = null;
		else
			this.series_id = Integer.parseInt(parts[4]);
		this.season_number = Integer.parseInt(parts[5]);
		this.episode_number = Integer.parseInt(parts[6]);
        this.series_years = parts[7];
        this.kind = parts[8];		
	}
	
	public void insert(Connection conn) throws SQLException{
		
		String query = "INSERT INTO Production (id, genre, title, production_year, series_id, season_number, episode_number, series_years, kind) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.setInt (1, id);
        preparedStmt.setString (2, genre);
        preparedStmt.setString (3, title);
        preparedStmt.setInt (4, production_year);
        if(series_id == null)
        	preparedStmt.setNull(5, Types.INTEGER);
        else
        	preparedStmt.setInt (5, series_id);
        preparedStmt.setInt (6, season_number);
        preparedStmt.setInt (7, episode_number);
        preparedStmt.setString (8, series_years);
        preparedStmt.setString (9, kind);        
        preparedStmt.execute();
	
	}
	
	public String toString()
	{
		return id + "\t" + genre + "\t" + title + "\t" + production_year + "\t" + series_id + "\t" + season_number + "\t" + episode_number + "\t" + series_years + "\t" + kind;
	}

}

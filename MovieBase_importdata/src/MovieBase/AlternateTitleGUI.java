// ID VE PRODUCTION ID 

package MovieBase;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class AlternateTitleGUI {

	private JFrame frame;
	private JTable table;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextArea textArea;
	private static Connection conn;
	
	private String columnNames[] = {"#", "ID", "Production ID", "Title" };
	private String[][] dataValues;
	private final static int COL_NO = 4;
	private final static int MAX_ROW_NO = 100;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AlternateTitleGUI window = new AlternateTitleGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AlternateTitleGUI() 
	{
		database_initialize();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		frame.setTitle("Alternative Title");
		frame.setBounds(100, 100, 773, 575);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		
		dataValues = new String[MAX_ROW_NO][COL_NO];
		initializeTable(dataValues);
		
		table = new JTable(dataValues, columnNames);
		table.setBounds(12, 13, 709, 215);
		table.getColumnModel().getColumn(0).setMaxWidth(30);
		frame.getContentPane().add(table);
		
		JLabel lblEnterTheInformation = new JLabel("Enter the information for the Alternative Title you want to add...");
		lblEnterTheInformation.setBounds(12, 251, 731, 16);
		frame.getContentPane().add(lblEnterTheInformation);
		
		JLabel lblOrEnter = new JLabel("... or enter a query to either display a result or delete [Table name: Alternative_title]");
		lblOrEnter.setBounds(12, 353, 731, 16);
		frame.getContentPane().add(lblOrEnter);
		
		JButton button = new JButton("< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				MovieBaseGUI mbGUI = new MovieBaseGUI();
			}
		});
		button.setBounds(12, 502, 97, 25);
		frame.getContentPane().add(button);
		
		JButton btnDisplayQuery = new JButton("Run Query");
		btnDisplayQuery.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnDisplayQuery.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String query = textArea.getText();
				query = query.toLowerCase();
				
				if(query.compareTo("") == 0)
					JOptionPane.showMessageDialog(frame, "Please enter a query!", "Error", JOptionPane.ERROR_MESSAGE);
				else
				{
					if(query.contains("delete"))
					{
						try {
							PreparedStatement preparedStmt = conn.prepareStatement(query);
					    	preparedStmt.execute();
						} catch (SQLException e1) 
						{
							e1.printStackTrace();
							JOptionPane.showMessageDialog(frame, "An error occurred!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
					else
					{
						try 
						{
							Statement stmt = conn.createStatement();
							stmt.setMaxRows(1000);
							ResultSet rs = stmt.executeQuery(query);
							
							initializeTable(dataValues);
							
							for(int i = 0; i < MAX_ROW_NO && rs.next(); i++)
					    	{
					    	    int id = rs.getInt("id");
					    	    int production_id = rs.getInt("production_id");
					    	    String title = rs.getString("title");
					    	    
					    	    for(int j = 0; j < COL_NO; j++)
								{
									if(j == 0)
										dataValues[i][j] = Integer.toString(i+1);
									if(j == 1)
										dataValues[i][j] = Integer.toString(id);
									if(j == 2)
										dataValues[i][j] = Integer.toString(production_id);
									if(j == 3)
										dataValues[i][j] = title;	
								}
					    	}
							
							table.setModel(new DefaultTableModel(dataValues, columnNames));
							table.getColumnModel().getColumn(0).setMaxWidth(30);
							
						} catch (SQLException e1) 
						{
							e1.printStackTrace();
							JOptionPane.showMessageDialog(frame, "An error occurred!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});
		btnDisplayQuery.setBounds(608, 502, 135, 25);
		frame.getContentPane().add(btnDisplayQuery);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblId.setForeground(Color.RED);
		lblId.setBounds(38, 281, 89, 16);
		frame.getContentPane().add(lblId);
		
		JLabel lblCountryCode = new JLabel("Production ID:");
		lblCountryCode.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCountryCode.setForeground(Color.RED);
		lblCountryCode.setBounds(394, 281, 102, 16);
		frame.getContentPane().add(lblCountryCode);
		
		JLabel lblName = new JLabel("Title:");
		lblName.setBounds(38, 319, 90, 16);
		frame.getContentPane().add(lblName);
		
		textField_1 = new JTextField();
		textField_1.setBounds(139, 277, 213, 25);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(139, 315, 604, 25);
		frame.getContentPane().add(textField_2);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(508, 278, 235, 25);
		frame.getContentPane().add(textField);
		
		textArea = new JTextArea();
		textArea.setBounds(12, 382, 731, 107);
		frame.getContentPane().add(textArea);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(12, 13, 731, 215);
		frame.getContentPane().add(scrollPane);
		
		JButton btnAdd = new JButton("Add Item");
        btnAdd.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) 
        	{
        		if((textField_1.getText().compareTo("")== 0) || (textField.getText().compareTo("")== 0))
        		{
        			if((textField_1.getText().compareTo("")== 0) && (textField.getText().compareTo("")== 0))
        				JOptionPane.showMessageDialog(frame, "Please enter values for ID and Production ID!", "Error", JOptionPane.ERROR_MESSAGE);
        			else
        				if(textField_1.getText().compareTo("")== 0)
        					JOptionPane.showMessageDialog(frame, "Please enter a value for ID!", "Error", JOptionPane.ERROR_MESSAGE);
        				else
        					JOptionPane.showMessageDialog(frame, "Please enter a value for Production ID!", "Error", JOptionPane.ERROR_MESSAGE);
        		}
				else
				{
					String[] parts = new String[3];
					parts[0] = textField_1.getText();
					parts[1] = textField_2.getText();
					parts[2] = textField.getText();
					
					AlternateTitle altTitle = new AlternateTitle(parts);
					try {
						altTitle.insert(conn);
					} catch (SQLException e1) 
					{
						e1.printStackTrace();
						JOptionPane.showMessageDialog(frame, "Cannot add the item!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
        	}
        });
        btnAdd.setFont(new Font("Tahoma", Font.PLAIN, 15));
        btnAdd.setBounds(452, 502, 135, 25);
        frame.getContentPane().add(btnAdd);		
	}
	
	private void initializeTable(String[][] dataValues)
	{
		for(int i = 0; i < dataValues.length; i++)
		{
			for(int j = 0; j < dataValues[0].length; j++)
			{
				if(j == 0)
					dataValues[i][j] = Integer.toString(i+1);
				else
					dataValues[i][j] = "";
			}
		}
	}
	
	private void database_initialize()
	{
    	String pass = "";
        try {
           create_connection("MovieBase", "root", pass);
        } catch (SQLException e) 
        {
        	JOptionPane.showMessageDialog(frame, "Cannot connect to database!", "Error", JOptionPane.ERROR_MESSAGE);
			MovieBaseGUI mbGUI = new MovieBaseGUI();
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }
	
	private void create_connection(String dbName,String dbUserName,String dbPassword) throws SQLException, ClassNotFoundException 
	{
        Class.forName("com.mysql.jdbc.Driver");
        String connectionString = "jdbc:mysql://localhost:3306/" + dbName + "?user=" + dbUserName + "&password=" + dbPassword ;
        conn = DriverManager.getConnection(connectionString);	
    }
}

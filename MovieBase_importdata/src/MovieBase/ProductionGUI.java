package MovieBase;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.SqlDateModel;

public class ProductionGUI 
{
	private JFrame frame;
	private JTable table;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextArea textArea;
	private JTextField textField;
	private JTextField textField_5;
	private JComboBox comboBox;
	private JComboBox comboBox_1;
	private JComboBox comboBox_2;
	private static Connection conn;
	
	private String columnNames[] = {"#", "ID", "Genre", "Title", "Production Year", "Series ID", "Season No", "Episode No", "Series Years", "Kind"};
	private String[][] dataValues;
	private final static int COL_NO = 10;
	private final static int MAX_ROW_NO = 100;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProductionGUI window = new ProductionGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ProductionGUI() 
	{
		database_initialize();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		frame.setTitle("Production");
		frame.setBounds(100, 100, 775, 660);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);		
		
		dataValues = new String[MAX_ROW_NO][COL_NO];
		initializeTable(dataValues);
		
		table = new JTable(dataValues, columnNames);
		table.setBounds(12, 13, 709, 215);
		table.getColumnModel().getColumn(0).setMaxWidth(30);
		frame.getContentPane().add(table);
		
		JLabel lblEnterTheInformation = new JLabel("Enter the information for the Production you want to add...");
		lblEnterTheInformation.setBounds(12, 251, 745, 16);
		frame.getContentPane().add(lblEnterTheInformation);
		
		JLabel lblOrEnter = new JLabel("... or enter a query to either display a result or delete [Table name: Production]");
		lblOrEnter.setBounds(12, 416, 745, 16);
		frame.getContentPane().add(lblOrEnter);
		
		JButton button = new JButton("< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				MovieBaseGUI mbGUI = new MovieBaseGUI();
			}
		});
		button.setBounds(12, 587, 97, 25);
		frame.getContentPane().add(button);
		
		JButton btnDisplayQuery = new JButton("Run Query");
		btnDisplayQuery.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnDisplayQuery.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String query = textArea.getText();
				query = query.toLowerCase();
				
				if(query.compareTo("") == 0)
					JOptionPane.showMessageDialog(frame, "Please enter a query!", "Error", JOptionPane.ERROR_MESSAGE);
				else
				{
					if(query.contains("delete"))
					{
						try {
							PreparedStatement preparedStmt = conn.prepareStatement(query);
					    	preparedStmt.execute();
						} catch (SQLException e1) 
						{
							e1.printStackTrace();
							JOptionPane.showMessageDialog(frame, "An error occurred!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
					else
					{
						try 
						{
							Statement stmt = conn.createStatement();
							stmt.setMaxRows(1000);
							ResultSet rs = stmt.executeQuery(query);
							
							initializeTable(dataValues);
							
							for(int i = 0; i < MAX_ROW_NO && rs.next(); i++)
					    	{
								int id = rs.getInt("id");
					    	    String genre = rs.getString("genre");
					    	    String title = rs.getString("title");
					    	    int production_year = rs.getInt("production_year");
					    	    int series_id = rs.getInt("series_id");
					    	    int season_number = rs.getInt("season_number");
					    	    int episode_number = rs.getInt("episode_number");
					    	    String series_years = rs.getString("series_years");
					    	    String kind = rs.getString("kind");
					    	    
					    	    for(int j = 0; j < COL_NO; j++)
								{
									if(j == 0)
										dataValues[i][j] = Integer.toString(i+1);
									if(j == 1)
										dataValues[i][j] = Integer.toString(id);
									if(j == 2)
										dataValues[i][j] = genre;
									if(j == 3)
										dataValues[i][j] = title;
									if(j == 4)
										dataValues[i][j] = Integer.toString(production_year);
									if(j == 5)
										dataValues[i][j] = Integer.toString(series_id);
									if(j == 6)
										dataValues[i][j] = Integer.toString(season_number);
									if(j == 7)
										dataValues[i][j] = Integer.toString(episode_number);
									if(j == 8)
										dataValues[i][j] = series_years;
									if(j == 9)
										dataValues[i][j] = kind;
								}
					    	}
							
							table.setModel(new DefaultTableModel(dataValues, columnNames));
							table.getColumnModel().getColumn(0).setMaxWidth(30);
							
						} catch (SQLException e1) 
						{
							e1.printStackTrace();
							JOptionPane.showMessageDialog(frame, "An error occurred!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});
		btnDisplayQuery.setBounds(622, 586, 135, 25);
		frame.getContentPane().add(btnDisplayQuery);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblId.setForeground(Color.RED);
		lblId.setBounds(38, 281, 46, 16);
		frame.getContentPane().add(lblId);
		
		JLabel lblName = new JLabel("Genre:");
		lblName.setBounds(242, 281, 46, 16);
		frame.getContentPane().add(lblName);
		
		textField_1 = new JTextField();
		textField_1.setBounds(82, 277, 127, 25);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(300, 277, 191, 25);
		frame.getContentPane().add(textField_2);
		
		textArea = new JTextArea();
		textArea.setBounds(12, 459, 745, 107);
		frame.getContentPane().add(textArea);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(12, 13, 731, 215);
		frame.getContentPane().add(scrollPane);
		
		JButton btnAdd = new JButton("Add Item");
        btnAdd.addActionListener(new ActionListener() 
        {
        	public void actionPerformed(ActionEvent e) 
        	{
        		if((textField_1.getText().compareTo("")== 0))
					JOptionPane.showMessageDialog(frame, "Please enter a value for ID!", "Error", JOptionPane.ERROR_MESSAGE);
				else
				{					
					String[] parts = new String[9];
					parts[0] = textField_1.getText();
					parts[1] = textField_2.getText();
					parts[2] = textField.getText();
					parts[3] = comboBox.getSelectedItem().toString();
					parts[4] = textField_5.getText();
					parts[5] = comboBox_1.getSelectedItem().toString();
					parts[6] = comboBox_2.getSelectedItem().toString();					
					parts[7] = textField_3.getText();
					parts[8] = textField_4.getText();

					Production production = new Production(parts);
					try {
						production.insert(conn);
					} catch (SQLException e1) 
					{
						e1.printStackTrace();
						JOptionPane.showMessageDialog(frame, "Cannot add the item!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
        	}
        });
        btnAdd.setFont(new Font("Tahoma", Font.PLAIN, 15));
        btnAdd.setBounds(475, 586, 135, 25);
        frame.getContentPane().add(btnAdd);
        
        JLabel lblGender = new JLabel("Title:");
        lblGender.setBounds(517, 281, 39, 16);
        frame.getContentPane().add(lblGender);
        
        JLabel lblTrivia = new JLabel("Production Year:");
        lblTrivia.setBounds(38, 325, 97, 16);
        frame.getContentPane().add(lblTrivia);
        
        JLabel lblQuotes = new JLabel("Season Number:");
        lblQuotes.setBounds(270, 325, 102, 16);
        frame.getContentPane().add(lblQuotes);
        
        JLabel lblBirthdate = new JLabel("Episode Number:");
        lblBirthdate.setBounds(517, 325, 102, 16);
        frame.getContentPane().add(lblBirthdate);
        
        SqlDateModel model = new SqlDateModel();
        JDatePanelImpl datePanel = new JDatePanelImpl(model);
                
        textField = new JTextField();
        textField.setColumns(10);
        textField.setBounds(568, 278, 189, 25);
        frame.getContentPane().add(textField);
        
        SqlDateModel model2 = new SqlDateModel();
        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2);
        
        JLabel lblBirthname = new JLabel("Series ID:");
        lblBirthname.setBounds(38, 370, 63, 16);
        frame.getContentPane().add(lblBirthname);
        
        textField_5 = new JTextField();
        textField_5.setColumns(10);
        textField_5.setBounds(113, 366, 127, 25);
        frame.getContentPane().add(textField_5);
        
        JLabel lblMiniBiography = new JLabel("Series Years:");
        lblMiniBiography.setBounds(270, 370, 97, 16);
        frame.getContentPane().add(lblMiniBiography);
        
        JLabel lblHeight = new JLabel("Kind:");
        lblHeight.setBounds(519, 370, 46, 16);
        frame.getContentPane().add(lblHeight);
        ComboBoxModel model1 = new DefaultComboBoxModel(initializeYear());
        ComboBoxModel model12 = new DefaultComboBoxModel(initializeSaENo());
        
        comboBox = new JComboBox();
        comboBox.setBounds(147, 322, 93, 22);
        frame.getContentPane().add(comboBox);
        comboBox.setModel(model1);
        
        comboBox_1 = new JComboBox();
        comboBox_1.setBounds(383, 322, 108, 22);
        frame.getContentPane().add(comboBox_1);
        comboBox_1.setModel(model12);
        
        comboBox_2 = new JComboBox();
        comboBox_2.setBounds(647, 322, 110, 22);
        frame.getContentPane().add(comboBox_2);
        comboBox_2.setModel(model12);
        
        textField_3 = new JTextField();
        textField_3.setColumns(10);
        textField_3.setBounds(364, 367, 127, 25);
        frame.getContentPane().add(textField_3);
        
        textField_4 = new JTextField();
        textField_4.setColumns(10);
        textField_4.setBounds(568, 367, 189, 25);
        frame.getContentPane().add(textField_4);        
	}
	
	private String[] initializeYear()
	{
		String[] years = new String[2016];
		
		for(int i = 0; i < years.length; i++)
		{
			years[i] = Integer.toString(years.length - i - 1);
		}
		
		return years;	
	}
	
	private String[] initializeSaENo()
	{
		String[] nos = new String[1001];
		
		for(int i = 0; i < nos.length; i++)
		{
			nos[i] = Integer.toString(i);
		}
		
		return nos;
	}
	
	private void initializeTable(String[][] dataValues)
	{
		for(int i = 0; i < dataValues.length; i++)
		{
			for(int j = 0; j < dataValues[0].length; j++)
			{
				if(j == 0)
					dataValues[i][j] = Integer.toString(i+1);
				else
					dataValues[i][j] = "";
			}
		}
	}
	
	private void database_initialize()
	{
    	String pass = "";
        try {
           create_connection("MovieBase", "root", pass);
        } catch (SQLException e) 
        {
        	JOptionPane.showMessageDialog(frame, "Cannot connect to database!", "Error", JOptionPane.ERROR_MESSAGE);
			MovieBaseGUI mbGUI = new MovieBaseGUI();
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }
	
	private void create_connection(String dbName,String dbUserName,String dbPassword) throws SQLException, ClassNotFoundException 
	{
        Class.forName("com.mysql.jdbc.Driver");
        String connectionString = "jdbc:mysql://localhost:3306/" + dbName + "?user=" + dbUserName + "&password=" + dbPassword ;
        conn = DriverManager.getConnection(connectionString);	
    }
}

package MovieBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
//Example Class
public class ProductionCast implements Insertable
{
	//private int cast_id;
    private Integer person_id;
    private Integer production_id;
    private String role_name;
    private Integer character_id;
    
    public ProductionCast(String[] parts)
    {
    	//this.cast_id = Integer.parseInt(parts[0]);
    	if(parts[0].compareTo("") == 0)
			this.person_id = null;
		else
			this.person_id = Integer.parseInt(parts[0]);
    	
    	if(parts[1].compareTo("") == 0)
			this.production_id = null;
		else
			this.production_id = Integer.parseInt(parts[1]);
    	
        this.role_name = parts[2];
        
        if(parts[3].compareTo("") == 0)
			this.character_id = null;
		else
			this.character_id = Integer.parseInt(parts[3]);
    }

    public void insert(Connection conn) throws SQLException{

        String query = "INSERT INTO Production_cast (person_id, production_id, role_name, character_id) VALUES (?, ?, ?, ?)";

        PreparedStatement preparedStmt = conn.prepareStatement(query);
        
        if(person_id == null)
        	preparedStmt.setNull(1, Types.INTEGER);
        else
        	preparedStmt.setInt (1, person_id);
        
        if(production_id == null)
        	preparedStmt.setNull(2, Types.INTEGER);
        else
        	preparedStmt.setInt (2, production_id);
        
        preparedStmt.setString (3, role_name);
        
        if(character_id == null)
        	preparedStmt.setNull(4, Types.INTEGER);
        else
        	preparedStmt.setInt (4, character_id);
        preparedStmt.execute();
    }

    public String toString(){
        return this.person_id + "\t" + this.production_id + "\t" + this.role_name + "\t" + this.character_id;

    }
}

package MovieBase;

import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MovieBaseGUI 
{
	private JFrame frmDfsf;
	private static Connection conn;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MovieBaseGUI window = new MovieBaseGUI();
					window.frmDfsf.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MovieBaseGUI() 
	{		
		database_initialize();
		initialize();
	}

	
	private void initialize() 
	{
		frmDfsf = new JFrame();
		frmDfsf.setResizable(false);
		frmDfsf.setTitle("Movie Database");
		frmDfsf.setBounds(100, 100, 555, 500);
		frmDfsf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDfsf.getContentPane().setLayout(null);
		frmDfsf.setVisible(true);
		frmDfsf.setLocationRelativeTo(null);
		
		JLabel lblNewLabel = new JLabel("Hello, Welcome to the Movie Database Program!");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setBounds(0, 0, 537, 45);
		frmDfsf.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Please choose the table that you want to interact...");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(72, 58, 394, 22);
		frmDfsf.getContentPane().add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("Company");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				frmDfsf.setVisible(false);
				try {
					CompanyGUI compGUI = new CompanyGUI();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(12, 111, 248, 59);
		frmDfsf.getContentPane().add(btnNewButton);
		
		JButton btnPerson = new JButton("Person");
		btnPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				frmDfsf.setVisible(false);
				PersonGUI personGUI = new PersonGUI();
			}
		});
		btnPerson.setBounds(277, 111, 248, 59);
		frmDfsf.getContentPane().add(btnPerson);
		
		JButton btnProduction = new JButton("Production");
		btnProduction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				frmDfsf.setVisible(false);
				ProductionGUI productionGUI = new ProductionGUI();
			}
		});
		btnProduction.setBounds(12, 183, 248, 59);
		frmDfsf.getContentPane().add(btnProduction);
		
		JButton btnCharacter = new JButton("Character");
		btnCharacter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				frmDfsf.setVisible(false);
				CharacterGUI characterGUI = new CharacterGUI();
			}
		});
		btnCharacter.setBounds(277, 183, 248, 59);
		frmDfsf.getContentPane().add(btnCharacter);
		
		JButton btnAlternativeTitle = new JButton("Alternative Title");
		btnAlternativeTitle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				frmDfsf.setVisible(false);
				AlternateTitleGUI altTitleGUI = new AlternateTitleGUI();
			}
		});
		btnAlternativeTitle.setBounds(12, 255, 248, 59);
		frmDfsf.getContentPane().add(btnAlternativeTitle);
		
		JButton btnAlternativeName = new JButton("Alternative Name");
		btnAlternativeName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				frmDfsf.setVisible(false);
				AlternativeNameGUI altNameGUI = new AlternativeNameGUI();
			}
		});
		btnAlternativeName.setBounds(277, 255, 248, 59);
		frmDfsf.getContentPane().add(btnAlternativeName);
		
		JButton btnProductionCast = new JButton("Production Cast");
		btnProductionCast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				frmDfsf.setVisible(false);
				ProductionCastGUI prodCastGUI = new ProductionCastGUI();
			}
		});
		btnProductionCast.setBounds(12, 327, 248, 59);
		frmDfsf.getContentPane().add(btnProductionCast);
		
		JButton btnProductionCompany = new JButton("Production Company");
		btnProductionCompany.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				frmDfsf.setVisible(false);
				ProductionCompanyGUI prodCompanyGUI = new ProductionCompanyGUI();
			}
		});
		btnProductionCompany.setBounds(277, 327, 248, 59);
		frmDfsf.getContentPane().add(btnProductionCompany);
		
		JButton btnCredits = new JButton("Credits");
		btnCredits.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				frmDfsf.setVisible(false);
				CreditsGUI creditsGUI = new CreditsGUI();
			}
		});
		btnCredits.setBounds(203, 402, 133, 38);
		frmDfsf.getContentPane().add(btnCredits);
		
		ImageIcon refresh = new ImageIcon("images/refresh.png");
		Image img = refresh.getImage();  
		Image newimg = img.getScaledInstance( 30, 30,  java.awt.Image.SCALE_SMOOTH ) ;  
		refresh = new ImageIcon( newimg );
		JButton btnNewButton_1 = new JButton(refresh);
		btnNewButton_1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				PreparedStatement preparedStmt;
				
				String query = "DROP TABLE IF EXISTS GenreProduction;";			
				
				try 
				{
					preparedStmt = conn.prepareStatement(query);
			    	preparedStmt.execute();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frmDfsf, "An error occurred at step 1!", "Error", JOptionPane.ERROR_MESSAGE);
				}
				
				query = "CREATE TABLE GenreProduction (countProductionCompany int(11) NOT NULL DEFAULT 0, companyName char(255) DEFAULT NULL, pyear int(11) NOT NULL, pgenre char(50) NOT NULL, KEY(pyear,pgenre)) ENGINE=MyISAM SELECT COUNT(production_company.production_id) AS countProductionCompany, company.name AS companyName, Production.production_year AS pyear, production.genre AS pgenre FROM Company INNER JOIN production_company ON production_company.company_id = Company.id INNER JOIN Production ON production_company.production_id = Production.id WHERE production.production_year IS NOT NULL AND production.genre IS NOT NULL GROUP BY (Production.production_year),production.genre,Production_company.company_id;";
				
				try 
				{
					preparedStmt = conn.prepareStatement(query);
			    	preparedStmt.execute();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frmDfsf, "An error occurred at step 2!", "Error", JOptionPane.ERROR_MESSAGE);
				}
				
				query = "DROP TABLE IF EXISTS MinBirthDate;";
				
				try 
				{
					preparedStmt = conn.prepareStatement(query);
			    	preparedStmt.execute();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frmDfsf, "An error occurred  at step 3!", "Error", JOptionPane.ERROR_MESSAGE);
				}
				
				query = "CREATE TABLE MinBirthDate (pid int(11) NOT NULL, minBirthDate DATE NOT NULL, KEY(pid,minBirthDate)) ENGINE=MyISAM SELECT MIN(person.birthdate) AS minBirthDate, production_cast.production_id AS pid FROM person INNER JOIN production_cast ON production_cast.person_id = person.id WHERE birthdate IS NOT NULL AND (role_name = 'actress'  OR role_name = 'actor') GROUP BY production_cast.production_id;";
			
				try 
				{
					preparedStmt = conn.prepareStatement(query);
			    	preparedStmt.execute();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frmDfsf, "An error occurred  at step 4!", "Error", JOptionPane.ERROR_MESSAGE);
				}
				
				query = "DROP TABLE IF EXISTS person_per_production;";
				
				try 
				{
					preparedStmt = conn.prepareStatement(query);
			    	preparedStmt.execute();
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frmDfsf, "An error occurred  at step 5!", "Error", JOptionPane.ERROR_MESSAGE);
				}
				
				query = "CREATE TABLE person_per_production (numOfPerson int(11) NOT NULL, pyear int(11), pid int(11) NOT NULL, KEY(pyear,numOfPerson)) ENGINE=MyISAM SELECT COUNT(pc.person_id) AS numOfPerson, p.production_year AS pyear, p.id AS pid FROM production_cast AS pc INNER JOIN production AS p ON p.id = pc.production_id GROUP BY p.id;";			
				
				try 
				{
					preparedStmt = conn.prepareStatement(query);
			    	preparedStmt.execute();
			    	JOptionPane.showMessageDialog(frmDfsf, "Database refreshed!", "Info", JOptionPane.INFORMATION_MESSAGE);
				} 
				catch (SQLException e1) 
				{
					e1.printStackTrace();
					JOptionPane.showMessageDialog(frmDfsf, "An error occurred at step 6!", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton_1.setBounds(478, 427, 47, 25);
		frmDfsf.getContentPane().add(btnNewButton_1);
	}
	
	private void database_initialize()
	{
    	String pass = "";
        try {
           create_connection("MovieBase", "root", pass);
        } catch (SQLException e) 
        {
        	JOptionPane.showMessageDialog(frmDfsf, "Cannot connect to database!", "Error", JOptionPane.ERROR_MESSAGE);
			MovieBaseGUI mbGUI = new MovieBaseGUI();
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }
	
	private void create_connection(String dbName,String dbUserName,String dbPassword) throws SQLException, ClassNotFoundException 
	{
        Class.forName("com.mysql.jdbc.Driver");
        String connectionString = "jdbc:mysql://localhost:3306/" + dbName + "?user=" + dbUserName + "&password=" + dbPassword ;
        conn = DriverManager.getConnection(connectionString);	
    }
}

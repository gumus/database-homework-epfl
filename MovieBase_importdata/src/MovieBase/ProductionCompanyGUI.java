package MovieBase;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class ProductionCompanyGUI {

	private JFrame frame;
	private JTable table;
	private JTextField textField_1;
	private JTextArea textArea;
	private JTextField textField;
	private JTextField textField_2;
	private JTextField textField_3;
	private static Connection conn;
	
	private String columnNames[] = {"#", "ID", "Production ID", "Company ID", "Kind" };
	private String[][] dataValues;
	private final static int COL_NO = 5;
	private final static int MAX_ROW_NO = 100;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProductionCompanyGUI window = new ProductionCompanyGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ProductionCompanyGUI() 
	{
		database_initialize();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		frame.setTitle("Production Company");
		frame.setBounds(100, 100, 773, 575);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		
		dataValues = new String[MAX_ROW_NO][COL_NO];
		initializeTable(dataValues);
		
		table = new JTable(dataValues, columnNames);
		table.setBounds(12, 13, 709, 215);
		table.getColumnModel().getColumn(0).setMaxWidth(30);
		frame.getContentPane().add(table);
		
		JLabel lblEnterTheInformation = new JLabel("Enter the information for the Production Company you want to add...");
		lblEnterTheInformation.setBounds(12, 251, 731, 16);
		frame.getContentPane().add(lblEnterTheInformation);
		
		JLabel lblOrEnter = new JLabel("... or enter a query to either display a result or delete [Table name: Production_company]");
		lblOrEnter.setBounds(12, 353, 731, 16);
		frame.getContentPane().add(lblOrEnter);
		
		JButton button = new JButton("< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				MovieBaseGUI mbGUI = new MovieBaseGUI();
			}
		});
		button.setBounds(12, 502, 97, 25);
		frame.getContentPane().add(button);
		
		JButton btnDisplayQuery = new JButton("Run Query");
		btnDisplayQuery.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnDisplayQuery.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String query = textArea.getText();
				query = query.toLowerCase();
				
				if(query.compareTo("") == 0)
					JOptionPane.showMessageDialog(frame, "Please enter a query!", "Error", JOptionPane.ERROR_MESSAGE);
				else
				{
					if(query.contains("delete"))
					{
						try {
							PreparedStatement preparedStmt = conn.prepareStatement(query);
					    	preparedStmt.execute();
						} catch (SQLException e1) 
						{
							e1.printStackTrace();
							JOptionPane.showMessageDialog(frame, "An error occurred!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
					else
					{
						try 
						{
							Statement stmt = conn.createStatement();
							stmt.setMaxRows(1000);
							ResultSet rs = stmt.executeQuery(query);
							
							initializeTable(dataValues);
							
							for(int i = 0; i < MAX_ROW_NO && rs.next(); i++)
					    	{								
							    int id = rs.getInt("id");
					    	    int production_id = rs.getInt("production_id");
					    	    int company_id = rs.getInt("company_id");
					    	    String kind = rs.getString("kind");
					    	    
					    	    for(int j = 0; j < COL_NO; j++)
								{
									if(j == 0)
										dataValues[i][j] = Integer.toString(i+1);
									if(j == 1)
										dataValues[i][j] = Integer.toString(id);
									if(j == 2)
										dataValues[i][j] = Integer.toString(production_id);
									if(j == 3)
										dataValues[i][j] = Integer.toString(company_id);
									if(j == 4)
										dataValues[i][j] = kind;
								}
					    	}
							
							table.setModel(new DefaultTableModel(dataValues, columnNames));
							table.getColumnModel().getColumn(0).setMaxWidth(30);
							
						} catch (SQLException e1) 
						{
							e1.printStackTrace();
							JOptionPane.showMessageDialog(frame, "An error occurred!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});
		btnDisplayQuery.setBounds(608, 502, 135, 25);
		frame.getContentPane().add(btnDisplayQuery);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblId.setForeground(Color.RED);
		lblId.setBounds(38, 281, 97, 16);
		frame.getContentPane().add(lblId);
		
		textField_1 = new JTextField();
		textField_1.setBounds(147, 277, 229, 25);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textArea = new JTextArea();
		textArea.setBounds(12, 382, 731, 107);
		frame.getContentPane().add(textArea);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(12, 13, 731, 215);
		frame.getContentPane().add(scrollPane);
		
		JButton btnAdd = new JButton("Add Item");
        btnAdd.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) 
        	{
        		if((textField_1.getText().compareTo("")== 0) || (textField.getText().compareTo("")== 0) || (textField_2.getText().compareTo("")== 0))
        		{
        			JOptionPane.showMessageDialog(frame, "Please enter the necessary values!", "Error", JOptionPane.ERROR_MESSAGE);
        		}
				else
				{
					String[] parts = new String[4];
					parts[0] = textField_1.getText();
					parts[1] = textField.getText();
					parts[2] = textField_2.getText();
					parts[3] = textField_3.getText();
					
					ProductionCompany prodCompany = new ProductionCompany(parts);
					try {
						prodCompany.insert(conn);
					} catch (SQLException e1) 
					{
						e1.printStackTrace();
						JOptionPane.showMessageDialog(frame, "Cannot add the item!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
        	}
        });
        btnAdd.setFont(new Font("Tahoma", Font.PLAIN, 15));
        btnAdd.setBounds(452, 502, 135, 25);
        frame.getContentPane().add(btnAdd);
        
        JLabel lblPersonId = new JLabel("Production ID:");
        lblPersonId.setForeground(Color.RED);
        lblPersonId.setFont(new Font("Tahoma", Font.BOLD, 13));
        lblPersonId.setBounds(401, 281, 97, 16);
        frame.getContentPane().add(lblPersonId);
        
        textField = new JTextField();
        textField.setColumns(10);
        textField.setBounds(501, 278, 242, 25);
        frame.getContentPane().add(textField);
        
        textField_2 = new JTextField();
        textField_2.setColumns(10);
        textField_2.setBounds(147, 316, 229, 25);
        frame.getContentPane().add(textField_2);
        
        JLabel lblRoleName = new JLabel("Kind:");
        lblRoleName.setBounds(401, 319, 79, 16);
        frame.getContentPane().add(lblRoleName);
        
        textField_3 = new JTextField();
        textField_3.setColumns(10);
        textField_3.setBounds(501, 316, 242, 25);
        frame.getContentPane().add(textField_3);
        
        JLabel lblCompanyId = new JLabel("Company ID:");
        lblCompanyId.setForeground(Color.RED);
        lblCompanyId.setFont(new Font("Tahoma", Font.BOLD, 13));
        lblCompanyId.setBounds(38, 319, 97, 16);
        frame.getContentPane().add(lblCompanyId);
	}
	
	private void initializeTable(String[][] dataValues)
	{
		for(int i = 0; i < dataValues.length; i++)
		{
			for(int j = 0; j < dataValues[0].length; j++)
			{
				if(j == 0)
					dataValues[i][j] = Integer.toString(i+1);
				else
					dataValues[i][j] = "";
			}
		}
	}
	
	private void database_initialize()
	{
    	String pass = "";
        try {
           create_connection("MovieBase", "root", pass);
        } catch (SQLException e) 
        {
        	JOptionPane.showMessageDialog(frame, "Cannot connect to database!", "Error", JOptionPane.ERROR_MESSAGE);
			MovieBaseGUI mbGUI = new MovieBaseGUI();
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }
	
	private void create_connection(String dbName,String dbUserName,String dbPassword) throws SQLException, ClassNotFoundException 
	{
        Class.forName("com.mysql.jdbc.Driver");
        String connectionString = "jdbc:mysql://localhost:3306/" + dbName + "?user=" + dbUserName + "&password=" + dbPassword ;
        conn = DriverManager.getConnection(connectionString);	
    }
}

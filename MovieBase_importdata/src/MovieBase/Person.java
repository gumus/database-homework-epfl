package MovieBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.Types;
//Example Class
public class Person implements Insertable{

    private static final java.text.SimpleDateFormat dateForm = new java.text.SimpleDateFormat("dd M yyyy");
    
    private int id;
    private String name;
    private String gender;
    private String trivia;
    private String quotes;
    private java.sql.Date birthdate;
    private java.sql.Date deathdate;
    private String birthname;
    private String minibiography;
    private String spouse;
    private float height;

    public Person(String[] parts)
    {
    	int index = 0;
    	for (String string : parts) {
			if(string.equals("\\N"))
				parts[index] = null;
			index++;	
		}
        this.id = Integer.parseInt(parts[0]);
        this.name = parts[1];
        this.gender = parts[2];
        this.trivia = parts[3];
        this.quotes = parts[4];
        
        try{
            java.util.Date date = dateForm.parse(parts[5]);
            this.birthdate = new Date(date.getTime());
            date = dateForm.parse(parts[6]);
            this.deathdate = new Date(date.getTime());
        }
        catch (Exception e)
        {
        }
        
        this.birthname = parts[7];
        this.minibiography = parts[8];
        this.spouse = parts[9];
        
        if(parts[10] != null)
        { 
            String feet = parts[10].split("'")[0];
            String inches = null;
            if(parts[10].split("'").length > 1)
            {
                inches = parts[10].split("'")[1].replace("\"", "");
            }
            int feetI = 0;
            int inchesI = 0;
            try
            {
                feetI = Integer.parseInt(feet);
                try
                {
                    inchesI = Integer.parseInt(inches);
                }
                catch(Exception e) {
                }
                this.height = feetI + (inchesI/12.0000f);
            }
            catch(Exception e) {
            }
        }
    }
    
    public void insert(Connection conn) throws SQLException
    {
    	String query = "INSERT INTO Person (id, name, gender, trivia, quotes, birthdate, deathdate, birthname, minibiography, spouse, height) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"; 	
    	
        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.setInt (1, id);
        preparedStmt.setString (2, name);
        preparedStmt.setString (3, gender);
        preparedStmt.setString(4, trivia);
        preparedStmt.setString(5, quotes);
        preparedStmt.setDate(6, birthdate);
        preparedStmt.setDate(7, deathdate);
        preparedStmt.setString(8, birthname);
        preparedStmt.setString(9, minibiography);
        preparedStmt.setString(10, spouse);
        preparedStmt.setFloat(11, height);
        preparedStmt.execute();
    }

    public void insert_WEIRD(Connection conn) throws SQLException{
        if(birthdate == null && height == 0 && deathdate ==null)
            return;
        String query = "UPDATE Person SET birthdate = ?, deathdate = ?, height = ? WHERE 'id' = ?";

        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.setInt (4, id);
        /*
        preparedStmt.setString (2, name);
        preparedStmt.setString (3, gender);
        preparedStmt.setString (4, trivia);
        preparedStmt.setString (5, quotes);
        */
        preparedStmt.setDate (1, birthdate);
        preparedStmt.setDate (2, deathdate);
        /*
        preparedStmt.setString (8, birthname);
        preparedStmt.setString (9, minibiography);
        preparedStmt.setString (10, spouse);
        */
        if(Math.abs(height) < 0.1){
            preparedStmt.setNull(3, Types.FLOAT);
        }
        else{
            preparedStmt.setFloat (3, height);
        }
       
        preparedStmt.executeUpdate();
    }

    public String toString(){
        return this.id + "\t" + this.name + "\t" + this.gender +
               "\t" + this.trivia + "\t" + this.quotes + "\t" + this.birthdate.getTime() +
               "\t" + this.deathdate.getTime() + "\t" + this.birthname + "\t" + this.minibiography +
               "\t" + this.spouse + "\t" + this.height;
    }
    
    public String toString_WEIRD()
    {
    	 return /*this.id + "\t" + this.name + "\t" + this.gender +
                 "\t" + this.trivia + "\t" + this.quotes + "\t" + */this.birthdate.getTime() +
                 "\t" + this.deathdate.getTime() + /*"\t" + this.birthname + "\t" + this.minibiography +
                 "\t" + this.spouse +*/ "\t" + this.height;
    }
}

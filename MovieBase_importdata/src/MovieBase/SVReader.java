package MovieBase;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Statement;

//Seperated Values Reader
public class SVReader <T> {
    final Class<T> typeParameterClass;

    private String seperator;
    private String file;
    private int piece_length;

    public Boolean done = false;

    private BufferedReader buf;

    public SVReader(String file_path, String seperator, int piece_length, Class<T> typeParameterClass) throws FileNotFoundException{
        this.piece_length = piece_length;
        this.seperator = seperator;
        this.file = file_path;
        this.typeParameterClass = typeParameterClass;

        buf = new BufferedReader(new FileReader(file));

    }

    public void bulk_insert(Connection conn, String table) throws SQLException
    {
        Statement stmt = conn.createStatement();
        String query =
            "load data local infile '" + file + "' \n" +
            "   replace \n"+
            "   into table " + table + " \n"+
            "   columns terminated by '\\t' \n" +
            "   LINES TERMINATED BY '\n'" +
            "   (@col1, @col2, @col3, @col4, @col5, @col6, @col7, @col8, @col9, @col10, @col11)" +
            "   set id=@col1, name=@col2, gender=@col3, trivia=@col4, quotes=@col5,"+
            "   birthname=@col8, minibiography=@col9, spouse=@col10" ;
        stmt.execute(query);
    }

    public void read_insert(Connection conn) throws FileNotFoundException{
        ArrayList<T>  piece = null;

        while(this.done != Boolean.TRUE)
        {		 
            piece = this.read_piece();
            for(T self : piece)
            {
                try {
                    ((Insertable) self).insert(conn);
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            piece = null;
            System.gc();
        }
    }
    
    private ArrayList<T> read_piece() throws FileNotFoundException{
        ArrayList<T>list = new ArrayList<T>();
        
        BufferedReader br = this.buf;
        String line = "";
        double count = 0;
        try {
            while ((line = br.readLine()) != null && this.piece_length >= count) {
                String[] parts = line.split(this.seperator);
                T object = parseClass(parts, typeParameterClass); 
                list.add(object);
                count++;
            }
            if(this.piece_length > count)
            {
                this.done = Boolean.TRUE;
                
                if (br != null) {
                    try {
                        br.close();
                        br = null;
                    } catch (IOException e) {
                       e.printStackTrace();
                    }
                }
            }
            else { 
                this.buf = br;
            }
        } catch (IOException e) {
    //        e.printStackTrace();
        }
        return list;
    }

    //Dont forget to add here your new tables
    private T parseClass(String[] parts, Class<T> type){
        if(type == Company.class)
            return (T) new Company(parts);
        else if(type == Person.class)
            return (T) new Person(parts);
        else if(type == Character.class)
            return (T) new Character(parts);
        else if(type == AlternateTitle.class)
            return (T) new AlternateTitle(parts);
        else if(type == AlternativeName.class)
            return (T) new AlternativeName(parts);
        else if(type == ProductionCast.class)
            return (T) new ProductionCast(parts);
        else if(type == Production.class)
            return (T) new Production(parts);
        return null;

    }
}

package MovieBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
//Example Class
public class AlternateTitle implements Insertable{

    private int id;    
    private String title;
    private int production_id;
    
    public AlternateTitle(String[] parts){

        this.id = Integer.parseInt(parts[0]);
        this.title = parts[1];
        this.production_id = Integer.parseInt(parts[2]);
    }

    public void insert(Connection conn) throws SQLException{

        String query = "INSERT INTO Alternative_title (id, title, production_id) VALUES (?, ?, ?)";

        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.setInt (1, id);
        preparedStmt.setString (2, title);
        preparedStmt.setInt (3, production_id);
        preparedStmt.execute();
    }

    public String toString()
    {
        return this.id + "\t" + this.production_id + "\t" + this.title;
    }
}

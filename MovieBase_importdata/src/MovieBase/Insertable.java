package MovieBase;

import java.sql.Connection;
import java.sql.SQLException;

public interface Insertable {
	public void insert(Connection conn) throws SQLException;
}

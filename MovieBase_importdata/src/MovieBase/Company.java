package MovieBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
//Example Class
public class Company implements Insertable{

    private int id;
    private String country_code;
    private String name;
    
    public Company(String[] parts){

        this.id = Integer.parseInt(parts[0]);
        if(parts[1].indexOf('[') != -1)
        	this.country_code = parts[1];
        else
        	this.country_code = parts[1].replace("[", "").replace("]", "");
        this.name = parts[2];
    }

    public void insert(Connection conn) throws SQLException{

        String query = "INSERT INTO Company (id, country_code, name) VALUES (?, ?, ?)";

        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.setInt (1, id);
        preparedStmt.setString (2, country_code);
        preparedStmt.setString (3, name);
        preparedStmt.execute();
    }
        
    public String toString(){
        return this.id + "\t" + this.name + "\t" + this.country_code;

    }
}

package MovieBase;

import java.awt.EventQueue;
import java.awt.TextArea;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;
import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class CompanyGUI {

	private JFrame frmCompany;
	private JTable table;
	private JTextField textField_1;
	private JTextField textField_2;
	private JComboBox comboBox;
	private JTextArea textArea;
	private static Connection conn;
	
	private String columnNames[] = {"#", "ID", "Country Code", "Name" };
	private String[][] dataValues;
	private final static int COL_NO = 4;
	private final static int MAX_ROW_NO = 100;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompanyGUI window = new CompanyGUI();
					window.frmCompany.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws SQLException 
	 */
	public CompanyGUI() throws SQLException 
	{
		database_initialize();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException 
	{
		frmCompany = new JFrame();
		frmCompany.setTitle("Company");
		frmCompany.setBounds(100, 100, 773, 575);
		frmCompany.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCompany.setVisible(true);
		frmCompany.getContentPane().setLayout(null);
		frmCompany.setLocationRelativeTo(null);
		frmCompany.setResizable(false);
		
		dataValues = new String[MAX_ROW_NO][COL_NO];
		initializeTable(dataValues);
		
		table = new JTable(dataValues, columnNames);
		table.setBounds(12, 13, 709, 215);
		table.getColumnModel().getColumn(0).setMaxWidth(30);
		frmCompany.getContentPane().add(table);
		
		JLabel lblEnterTheInformation = new JLabel("Enter the information for the Company you want to add...");
		lblEnterTheInformation.setBounds(12, 251, 731, 16);
		frmCompany.getContentPane().add(lblEnterTheInformation);
		
		JLabel lblOrEnter = new JLabel("... or enter a query to either display a result or delete [Table name: Company]");
		lblOrEnter.setBounds(12, 353, 731, 16);
		frmCompany.getContentPane().add(lblOrEnter);
		
		JButton button = new JButton("< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmCompany.setVisible(false);
				MovieBaseGUI mbGUI = new MovieBaseGUI();
			}
		});
		button.setBounds(12, 502, 97, 25);
		frmCompany.getContentPane().add(button);
		
		JButton btnDisplayQuery = new JButton("Run Query");
		btnDisplayQuery.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnDisplayQuery.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String query = textArea.getText();
				query = query.toLowerCase();
				
				if(query.compareTo("") == 0)
					JOptionPane.showMessageDialog(frmCompany, "Please enter a query!", "Error", JOptionPane.ERROR_MESSAGE);
				else
				{
					if(query.contains("delete"))
					{
						try {
							PreparedStatement preparedStmt = conn.prepareStatement(query);
					    	preparedStmt.execute();
						} catch (SQLException e1) 
						{
							e1.printStackTrace();
							JOptionPane.showMessageDialog(frmCompany, "An error occurred!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
					else
					{
						try 
						{
							Statement stmt = conn.createStatement();
							stmt.setMaxRows(1000);
							ResultSet rs = stmt.executeQuery(query);
							
							initializeTable(dataValues);
							
							for(int i = 0; i < MAX_ROW_NO && rs.next(); i++)
					    	{
					    	    int id = rs.getInt("id");
					    	    String country_code = rs.getString("country_code");
					    	    String name = rs.getString("name");
					    	    
					    	    for(int j = 0; j < COL_NO; j++)
								{
									if(j == 0)
										dataValues[i][j] = Integer.toString(i+1);
									if(j == 1)
										dataValues[i][j] = Integer.toString(id);
									if(j == 2)
										dataValues[i][j] = country_code;
									if(j == 3)
										dataValues[i][j] = name;	
								}
					    	}
							
							table.setModel(new DefaultTableModel(dataValues, columnNames));
							table.getColumnModel().getColumn(0).setMaxWidth(30);
							
						} catch (SQLException e1) 
						{
							e1.printStackTrace();
							JOptionPane.showMessageDialog(frmCompany, "An error occurred!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});
		btnDisplayQuery.setBounds(608, 502, 135, 25);
		frmCompany.getContentPane().add(btnDisplayQuery);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblId.setForeground(Color.RED);
		lblId.setBounds(38, 281, 89, 16);
		frmCompany.getContentPane().add(lblId);
		
		JLabel lblCountryCode = new JLabel("Country Code:");
		lblCountryCode.setBounds(523, 281, 90, 16);
		frmCompany.getContentPane().add(lblCountryCode);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(38, 319, 90, 16);
		frmCompany.getContentPane().add(lblName);
		
		textField_1 = new JTextField();
		textField_1.setBounds(139, 277, 340, 25);
		frmCompany.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(139, 315, 604, 25);
		frmCompany.getContentPane().add(textField_2);
		
		textArea = new JTextArea();
		textArea.setBounds(12, 382, 731, 107);
		frmCompany.getContentPane().add(textArea);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(12, 13, 731, 215);
		frmCompany.getContentPane().add(scrollPane);
		
		comboBox = new JComboBox();
		comboBox.setBounds(646, 278, 97, 22);
		frmCompany.getContentPane().add(comboBox);
        ComboBoxModel model = new DefaultComboBoxModel(initializeCountryCodes(conn));
        comboBox.setModel(model);
        
        JButton btnAdd = new JButton("Add Item");
        btnAdd.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) 
        	{
        		if((textField_1.getText().compareTo("")== 0))
					JOptionPane.showMessageDialog(frmCompany, "Please enter a value for ID!", "Error", JOptionPane.ERROR_MESSAGE);
				else
				{
					String[] parts = new String[3];
					parts[0] = textField_1.getText();
					parts[1] = comboBox.getSelectedItem().toString();
					parts[2] = textField_2.getText();
					
					Company comp = new Company(parts);
					try {
						comp.insert(conn);
					} catch (SQLException e1) 
					{
						e1.printStackTrace();
						JOptionPane.showMessageDialog(frmCompany, "Cannot add the item!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
        	}
        });
        btnAdd.setFont(new Font("Tahoma", Font.PLAIN, 15));
        btnAdd.setBounds(452, 502, 135, 25);
        frmCompany.getContentPane().add(btnAdd);
	}
	
	private void initializeTable(String[][] dataValues)
	{
		for(int i = 0; i < dataValues.length; i++)
		{
			for(int j = 0; j < dataValues[0].length; j++)
			{
				if(j == 0)
					dataValues[i][j] = Integer.toString(i+1);
				else
					dataValues[i][j] = "";
			}
		}
	}
	
	private String[] initializeCountryCodes(Connection conn) throws SQLException
	{
		ArrayList<String> temp = new ArrayList<String>();
		
		Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery("SELECT DISTINCT country_code FROM company;");
    	
    	while(rs.next())
    	{
    		temp.add(rs.getString("country_code"));
    	}
		
		String[] countries = new String[temp.size()];
		
		for(int i = 0; i < countries.length; i++)
		{
			countries[i] = temp.get(i);
		}
		
		return countries;
	}
	
	private void database_initialize()
	{
    	String pass = "";
        try {
           create_connection("MovieBase", "root", pass);
        } catch (SQLException e) 
        {
        	JOptionPane.showMessageDialog(frmCompany, "Cannot connect to database!", "Error", JOptionPane.ERROR_MESSAGE);
			MovieBaseGUI mbGUI = new MovieBaseGUI();
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }
	
	private void create_connection(String dbName,String dbUserName,String dbPassword) throws SQLException, ClassNotFoundException 
	{
        Class.forName("com.mysql.jdbc.Driver");
        String connectionString = "jdbc:mysql://localhost:3306/" + dbName + "?user=" + dbUserName + "&password=" + dbPassword ;
        conn = DriverManager.getConnection(connectionString);	
    }
}

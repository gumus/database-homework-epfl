package MovieBase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
//Example Class
public class Character implements Insertable{

    private int id;
    private String name;
    
    public Character(String[] parts){

        this.id = Integer.parseInt(parts[0]);
        this.name = parts[1];

    }

    public void insert(Connection conn) throws SQLException{

        String query = "INSERT INTO Characters (id, name) VALUES (?, ?)";

        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.setInt (1, id);
        preparedStmt.setString (2, name);
        preparedStmt.execute();
    }

    public String toString(){
        return this.id + "\t" + this.name;

    }
}

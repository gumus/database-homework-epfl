// TAR�HLER T�RK�E!

package MovieBase;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.SqlDateModel;

public class PersonGUI 
{
	private static final java.text.SimpleDateFormat dateForm = new java.text.SimpleDateFormat("dd M yyyy");

	private JFrame frame;
	private JTable table;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextArea textArea;
	private JComboBox comboBox1;
	private JComboBox comboBox2;
	private JDatePickerImpl datePicker;
	private JDatePickerImpl datePicker2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private static Connection conn;
	
	private String columnNames[] = {"#", "ID", "Name", "Gender", "Trivia", "Quotes", "Birthdate", "Deathdate", "Birthname", "Minibiography", "Spouse", "Height"};
	private String[][] dataValues;
	private final static int COL_NO = 12;
	private final static int MAX_ROW_NO = 100;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PersonGUI window = new PersonGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PersonGUI() 
	{
		database_initialize();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Person");
		frame.setBounds(100, 100, 775, 707);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		
		dataValues = new String[MAX_ROW_NO][COL_NO];
		initializeTable(dataValues);
		
		table = new JTable(dataValues, columnNames);
		table.setBounds(12, 13, 709, 215);
		table.getColumnModel().getColumn(0).setMaxWidth(30);
		frame.getContentPane().add(table);
		
		JLabel lblEnterTheInformation = new JLabel("Enter the information for the Person you want to add...");
		lblEnterTheInformation.setBounds(12, 251, 745, 16);
		frame.getContentPane().add(lblEnterTheInformation);
		
		JLabel lblOrEnter = new JLabel("... or enter a query to either display a result or delete [Table name: Person]");
		lblOrEnter.setBounds(12, 485, 745, 16);
		frame.getContentPane().add(lblOrEnter);
		
		JButton button = new JButton("< Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				MovieBaseGUI mbGUI = new MovieBaseGUI();
			}
		});
		button.setBounds(12, 634, 97, 25);
		frame.getContentPane().add(button);
		
		JButton btnDisplayQuery = new JButton("Run Query");
		btnDisplayQuery.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnDisplayQuery.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String query = textArea.getText();
				query = query.toLowerCase();
				
				if(query.compareTo("") == 0)
					JOptionPane.showMessageDialog(frame, "Please enter a query!", "Error", JOptionPane.ERROR_MESSAGE);
				else
				{
					if(query.contains("delete"))
					{
						try {
							PreparedStatement preparedStmt = conn.prepareStatement(query);
					    	preparedStmt.execute();
						} catch (SQLException e1) 
						{
							e1.printStackTrace();
							JOptionPane.showMessageDialog(frame, "An error occurred!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
					else
					{
						try 
						{
							Statement stmt = conn.createStatement();
							stmt.setMaxRows(1000);
							ResultSet rs = stmt.executeQuery(query);
							
							initializeTable(dataValues);
							
							for(int i = 0; i < MAX_ROW_NO && rs.next(); i++)
					    	{
								int id = rs.getInt("id");
					    	    String name = rs.getString("name");
					    	    String gender = rs.getString("gender");
					    	    String trivia = rs.getString("trivia");
					    	    String quotes = rs.getString("quotes");
					    	    Date birthdate = rs.getDate("birthdate");
					    	    Date deathdate = rs.getDate("deathdate");
					    	    String birthname = rs.getString("birthname");
					    	    String minibiography = rs.getString("minibiography");
					    	    String spouse = rs.getString("spouse");
					    	    float height = rs.getFloat("height"); 
					    	    
					    	    for(int j = 0; j < COL_NO; j++)
								{
									if(j == 0)
										dataValues[i][j] = Integer.toString(i+1);
									if(j == 1)
										dataValues[i][j] = Integer.toString(id);
									if(j == 2)
										dataValues[i][j] = name;
									if(j == 3)
										dataValues[i][j] = gender;
									if(j == 4)
										dataValues[i][j] = trivia;
									if(j == 5)
										dataValues[i][j] = quotes;
									if(j == 6)
									{
										dataValues[i][j] = birthdate + "";
										if(dataValues[i][j].compareTo("null") == 0)
											dataValues[i][j] = "";
									}
									if(j == 7)
									{
										dataValues[i][j] = deathdate + "";
										if(dataValues[i][j].compareTo("null") == 0)
											dataValues[i][j] = "";
									}
									if(j == 8)
										dataValues[i][j] = birthname;
									if(j == 9)
										dataValues[i][j] = minibiography;
									if(j == 10)
										dataValues[i][j] = spouse;
									if(j == 11)
										dataValues[i][j] = Float.toString(height);
								}
					    	}
							
							table.setModel(new DefaultTableModel(dataValues, columnNames));
							table.getColumnModel().getColumn(0).setMaxWidth(30);
							
						} catch (SQLException e1) 
						{
							e1.printStackTrace();
							JOptionPane.showMessageDialog(frame, "An error occurred!", "Error", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});
		btnDisplayQuery.setBounds(622, 633, 135, 25);
		frame.getContentPane().add(btnDisplayQuery);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblId.setForeground(Color.RED);
		lblId.setBounds(38, 281, 46, 16);
		frame.getContentPane().add(lblId);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(242, 281, 46, 16);
		frame.getContentPane().add(lblName);
		
		textField_1 = new JTextField();
		textField_1.setBounds(82, 277, 127, 25);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(300, 277, 191, 25);
		frame.getContentPane().add(textField_2);
		
		textArea = new JTextArea();
		textArea.setBounds(12, 514, 745, 107);
		frame.getContentPane().add(textArea);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(12, 13, 731, 215);
		frame.getContentPane().add(scrollPane);
		
		JButton btnAdd = new JButton("Add Item");
        btnAdd.addActionListener(new ActionListener() 
        {
        	public void actionPerformed(ActionEvent e) 
        	{
        		if((textField_1.getText().compareTo("")== 0))
					JOptionPane.showMessageDialog(frame, "Please enter a value for ID!", "Error", JOptionPane.ERROR_MESSAGE);
				else
				{					
					String[] parts = new String[11];
					parts[0] = textField_1.getText();
					parts[1] = textField_2.getText();
					parts[2] = textField.getText();
					parts[3] = textField_3.getText();
					parts[4] = textField_4.getText();
					
					try
					{
						parts[5] = datePicker.getModel().getValue().toString();
					}
					catch(NullPointerException exc)
					{
						parts[5] = "\\N";
					}
					
					try
					{
						parts[6] = datePicker2.getModel().getValue().toString();
					}
					catch(NullPointerException exc)
					{
						parts[6] = "\\N";
					}
					
					parts[7] = textField_5.getText();
					parts[8] = textField_6.getText();
					parts[9] = textField_7.getText();
					parts[10] = comboBox1.getSelectedItem().toString() + "' " + comboBox2.getSelectedItem().toString() + "\"";
					
					Person person = new Person(parts);
					try {
						person.insert(conn);
					} catch (SQLException e1) 
					{
						e1.printStackTrace();
						JOptionPane.showMessageDialog(frame, "Cannot add the item!", "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
        	}
        });
        btnAdd.setFont(new Font("Tahoma", Font.PLAIN, 15));
        btnAdd.setBounds(475, 633, 135, 25);
        frame.getContentPane().add(btnAdd);
        
        JLabel lblGender = new JLabel("Gender:");
        lblGender.setBounds(517, 281, 90, 16);
        frame.getContentPane().add(lblGender);
        
        JLabel lblTrivia = new JLabel("Trivia:");
        lblTrivia.setBounds(38, 325, 46, 16);
        frame.getContentPane().add(lblTrivia);
        
        textField_3 = new JTextField();
        textField_3.setColumns(10);
        textField_3.setBounds(82, 321, 270, 25);
        frame.getContentPane().add(textField_3);
        
        JLabel lblQuotes = new JLabel("Quotes:");
        lblQuotes.setBounds(381, 325, 90, 16);
        frame.getContentPane().add(lblQuotes);
        
        textField_4 = new JTextField();
        textField_4.setColumns(10);
        textField_4.setBounds(439, 321, 318, 25);
        frame.getContentPane().add(textField_4);
        
        JLabel lblBirthdate = new JLabel("Birthdate:");
        lblBirthdate.setBounds(38, 372, 63, 16);
        frame.getContentPane().add(lblBirthdate);
        
        SqlDateModel model = new SqlDateModel();
        JDatePanelImpl datePanel = new JDatePanelImpl(model);
        datePicker = new JDatePickerImpl(datePanel);
        datePicker.setBounds(113, 363, 239, 27);
        frame.getContentPane().add(datePicker);
                
        textField = new JTextField();
        textField.setColumns(10);
        textField.setBounds(595, 278, 162, 25);
        frame.getContentPane().add(textField);
        
        JLabel label = new JLabel("Deathdate:");
        label.setBounds(381, 372, 63, 16);
        frame.getContentPane().add(label);
        
        SqlDateModel model2 = new SqlDateModel();
        JDatePanelImpl datePanel2 = new JDatePanelImpl(model2);
        datePicker2 = new JDatePickerImpl(datePanel2);
        datePicker2.setBounds(475, 361, 282, 27);
        frame.getContentPane().add(datePicker2);
        
        JLabel lblBirthname = new JLabel("Birthname:");
        lblBirthname.setBounds(38, 416, 63, 16);
        frame.getContentPane().add(lblBirthname);
        
        textField_5 = new JTextField();
        textField_5.setColumns(10);
        textField_5.setBounds(113, 413, 127, 25);
        frame.getContentPane().add(textField_5);
        
        JLabel lblMiniBiography = new JLabel("Mini Biography:");
        lblMiniBiography.setBounds(270, 416, 97, 16);
        frame.getContentPane().add(lblMiniBiography);
        
        textField_6 = new JTextField();
        textField_6.setColumns(10);
        textField_6.setBounds(372, 413, 385, 25);
        frame.getContentPane().add(textField_6);
        
        JLabel lblSpouse = new JLabel("Spouse:");
        lblSpouse.setBounds(38, 456, 63, 16);
        frame.getContentPane().add(lblSpouse);
        
        textField_7 = new JTextField();
        textField_7.setColumns(10);
        textField_7.setBounds(113, 447, 239, 25);
        frame.getContentPane().add(textField_7);
        
        JLabel lblHeight = new JLabel("Height:");
        lblHeight.setBounds(381, 456, 63, 16);
        frame.getContentPane().add(lblHeight);
        
        comboBox1 = new JComboBox();
		comboBox1.setBounds(439, 453, 71, 22);
		frame.getContentPane().add(comboBox1);
        ComboBoxModel model1 = new DefaultComboBoxModel(initializeHeightFeet());
        comboBox1.setModel(model1);
        
        JLabel lblM = new JLabel("' (feet)");
        lblM.setBounds(527, 456, 39, 16);
        frame.getContentPane().add(lblM);
        
        comboBox2 = new JComboBox();
		comboBox2.setBounds(588, 453, 97, 22);
		frame.getContentPane().add(comboBox2);
        ComboBoxModel model12 = new DefaultComboBoxModel(initializeHeightInch());
        comboBox2.setModel(model12);
        
        JLabel lblCm = new JLabel("\" (inch)");
        lblCm.setBounds(711, 456, 46, 16);
        frame.getContentPane().add(lblCm);		
	}
	
	private String[] initializeHeightFeet()
	{
		String[] height = new String[15];

		for(int i = 0; i < 15; i++)
		{
			height[i] = Integer.toString(i);
		}
		
		return height;
	}
	
	private String[] initializeHeightInch()
	{
		String[] height = new String[12];
		
		for(int i = 0; i < 12; i++)
		{
			height[i] = Integer.toString(i);
		}
		
		return height;
	}
	
	private void initializeTable(String[][] dataValues)
	{
		for(int i = 0; i < dataValues.length; i++)
		{
			for(int j = 0; j < dataValues[0].length; j++)
			{
				if(j == 0)
					dataValues[i][j] = Integer.toString(i+1);
				else
					dataValues[i][j] = "";
			}
		}
	}
	
	private void database_initialize()
	{
    	String pass = "";
        try {
           create_connection("MovieBase", "root", pass);
        } catch (SQLException e) 
        {
        	JOptionPane.showMessageDialog(frame, "Cannot connect to database!", "Error", JOptionPane.ERROR_MESSAGE);
			MovieBaseGUI mbGUI = new MovieBaseGUI();
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }        
    }
	
	private void create_connection(String dbName,String dbUserName,String dbPassword) throws SQLException, ClassNotFoundException 
	{
        Class.forName("com.mysql.jdbc.Driver");
        String connectionString = "jdbc:mysql://localhost:3306/" + dbName + "?user=" + dbUserName + "&password=" + dbPassword ;
        conn = DriverManager.getConnection(connectionString);	
    }
}

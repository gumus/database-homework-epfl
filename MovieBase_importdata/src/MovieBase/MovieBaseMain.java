package MovieBase;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.lang.Boolean;

class MovieBaseMain {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver"; 

    private static Connection conn; 
    private static final int PIECE = 100;
    
    public static void main(String[] args) throws ClassNotFoundException
    {    
    	database_initialize();
        menu();
    }
    
    private static void menu()
    {
    	Scanner input = new Scanner(System.in);
    	Boolean loop = Boolean.TRUE;
    	
    	while(loop)
    	{    		
    		System.out.println("Table List: \n1-Company\n2-Person\n3-Alternative Title\n4-Alternative Name\n5-Character\n6-Production Cast\n7-Production\n8-Production Company");
    		System.out.print("Choose the table you want to work on: ");
    		int option = input.nextInt();
    		System.out.println();
    		
    		 switch (option) {
             case 1:  withCompany();
                      break;
             case 2:  withPerson();
                      break;
             case 3:  withAlternateTitle();
                      break;
             case 4:  withAlternativeName();
                      break;
             case 5:  withCharacter();
                      break;
             case 6:  withProductionCast();
                      break;
             case 7:  withProduction();
             		  break;
             case 8:  withProductionCompany();
             		  break;
             default: loop = false;
                      break;
    		 }
    	}
    	
    	System.out.println("Have a nice day!");
    	
    	input.close();
    }

    private static void withCompany()
    {
    	Scanner scan = new Scanner(System.in);
    	System.out.println("1-Add\n2-Delete\n3-Show");
    	System.out.print("What do you want to do? ");
    	int opt = scan.nextInt();
    	
    	String temp;
    	String[] parts;
    	
    	switch(opt)
    	{
    		case 1:
    			System.out.print("Enter the information respectively for the Company as shown [id,country_code,name]: ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	
    	    	parts = getInfo(temp);
    	    	
    	    	Company company = new Company(parts);
    	    	try {
    				company.insert(conn);
    				System.out.println();
    			} catch (SQLException e) {
    				e.printStackTrace();
    				System.out.println("ERROR!");
    			}
    	    break;
    	    
    		case 2:
    			System.out.print("Enter the deletion query for Company (id, country_code, name): ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	System.out.println();
    	    	
	    	    try {
					delete(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	    	
    		break;
    		
    		case 3:
    			System.out.println("Enter the query you want to display about Company (id, country_code, name): ");
    			scan.nextLine();
    			temp = scan.nextLine();
    			System.out.println();
    						    			
    			try {
					selectCompany(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	
    		break;
    		
    		default:
    			System.out.println("Wrong input!");
    		break;
    	}
    }
    
    private static void withPerson()
    {
    	Scanner scan = new Scanner(System.in);
    	System.out.println("1-Add\n2-Delete\n3-Show");
    	System.out.print("What do you want to do? ");
    	int opt = scan.nextInt();
    	
    	String temp;
    	String[] parts;
    	
    	switch(opt)
    	{
    		case 1:
    			System.out.print("Enter the information respectively for the Person as shown [id,name,gender,trivia,quotes,birthdate,deathdate,birthname,minibiography,spouse,height]: ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	
    	    	parts = getInfo(temp);
    	    	
    	    	Person person = new Person(parts);
    	    	try {
    				person.insert(conn);
    				System.out.println();
    			} catch (SQLException e) {
    				e.printStackTrace();
    				System.out.println("ERROR!");
    			}
    	    break;
    	    
    		case 2:
    			System.out.print("Enter the deletion query for Person (id, name, gender, trivia, quotes, birthdate, deathdate, birthname, minibiography, spouse, height): ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	System.out.println();
    	    	
	    	    try {
					delete(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	    	
    		break;
    		
    		case 3:
    			System.out.println("Enter the query you want to display about Person (id, name, gender, trivia, quotes, birthdate, deathdate, birthname, minibiography, spouse, height): ");
    			scan.nextLine();
    			temp = scan.nextLine();
    			System.out.println();
    			
    			try {
					selectPerson(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	
    		break;
    		
    		default:
    			System.out.println("Wrong input!");
    		break;
    	}    	
    }
   
    private static void withAlternateTitle()
    {
    	Scanner scan = new Scanner(System.in);
    	System.out.println("1-Add\n2-Delete\n3-Show");
    	System.out.print("What do you want to do? ");
    	int opt = scan.nextInt();
    	
    	String temp;
    	String[] parts;
    	
    	switch(opt)
    	{
    		case 1:
    			System.out.print("Enter the information respectively for the Alternative_Title as shown [id,production_id,title]: ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	
    	    	parts = getInfo(temp);
    	    	
    	    	AlternateTitle title = new AlternateTitle(parts);
    	    	try {
    				title.insert(conn);
    				System.out.println();
    			} catch (SQLException e) {
    				e.printStackTrace();
    				System.out.println("ERROR!");
    			}
    	    break;
    	    
    		case 2:
    			System.out.print("Enter the deletion query for Alternative_Title (id, production_id, title): ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	System.out.println();
    	    	
	    	    try {
					delete(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	    	
    		break;
    		
    		case 3:
    			System.out.println("Enter the query you want to display about Alternative_Title (id, production_id, title): ");
    			scan.nextLine();
    			temp = scan.nextLine();
    			System.out.println();
    			
    			try {
					selectAlternateTitle(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	
    		break;
    		
    		default:
    			System.out.println("Wrong input!");
    		break;
    	}    	
    }
    
    private static void withAlternativeName()
    {
    	Scanner scan = new Scanner(System.in);
    	System.out.println("1-Add\n2-Delete\n3-Show");
    	System.out.print("What do you want to do? ");
    	int opt = scan.nextInt();
    	
    	String temp;
    	String[] parts;
    	
    	switch(opt)
    	{
    		case 1:
    			System.out.print("Enter the information respectively for the Alternative_Name as shown [id,name,person_id]: ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	
    	    	parts = getInfo(temp);
    	    	
    	    	AlternativeName name = new AlternativeName(parts);
    	    	try {
    				name.insert(conn);
    				System.out.println();
    			} catch (SQLException e) {
    				e.printStackTrace();
    				System.out.println("ERROR!");
    			}
    	    break;
    	    
    		case 2:
    			System.out.print("Enter the deletion query for Alternative_Name (id, name, person_id): ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	System.out.println();
    	    	
	    	    try {
					delete(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	    	
    		break;
    		
    		case 3:
    			System.out.println("Enter the query you want to display about Alternative_Name (id, name, person_id): ");
    			scan.nextLine();
    			temp = scan.nextLine();
    			System.out.println();
    			
    			try {
					selectAlternativeName(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	
    		break;
    		
    		default:
    			System.out.println("Wrong input!");
    		break;
    	}
    }
    
    private static void withCharacter()
    {
    	Scanner scan = new Scanner(System.in);
    	System.out.println("1-Add\n2-Delete\n3-Show");
    	System.out.print("What do you want to do? ");
    	int opt = scan.nextInt();
    	
    	String temp;
    	String[] parts;
    	
    	switch(opt)
    	{
    		case 1:
    			System.out.print("Enter the information respectively for the Character as shown [id,name]: ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	
    	    	parts = getInfo(temp);
    	    	
    	    	Character character = new Character(parts);
    	    	try {
    				character.insert(conn);
    				System.out.println();
    			} catch (SQLException e) {
    				e.printStackTrace();
    				System.out.println("ERROR!");
    			}
    	    break;
    	    
    		case 2:
    			System.out.print("Enter the deletion query for Character (id, name): ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	System.out.println();
    	    	
	    	    try {
					delete(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	    	
    		break;
    		
    		case 3:
    			System.out.println("Enter the query you want to display about Character (id, name): ");
    			scan.nextLine();
    			temp = scan.nextLine();
    			System.out.println();
    			
    			try {
					selectCharacter(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	
    		break;
    		
    		default:
    			System.out.println("Wrong input!");
    		break;
    	}
    }
    
    private static void withProduction()
    {
    	Scanner scan = new Scanner(System.in);
    	System.out.println("1-Add\n2-Delete\n3-Show");
    	System.out.print("What do you want to do? ");
    	int opt = scan.nextInt();
    	
    	String temp;
    	String[] parts;
    	
    	switch(opt)
    	{
    		case 1:
    			System.out.print("Enter the information respectively for the Production as shown [id,genre,title,production_year,series_id,season_number,episode_number,series_years,kind]: ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	
    	    	parts = getInfo(temp);
    	    	
    	    	Production production = new Production(parts);
    	    	try {
    				production.insert(conn);
    				System.out.println();
    			} catch (SQLException e) {
    				e.printStackTrace();
    				System.out.println("ERROR!");
    			}
    	    break;
    	    
    		case 2:
    			System.out.print("Enter the deletion query for Production (id, genre, title, production_year, series_id, season_number, episode_number, series_years, kind): ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	System.out.println();
    	    	
	    	    try {
					delete(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	    	
    		break;
    		
    		case 3:
    			System.out.println("Enter the query you want to display about Production (id, genre, title, production_year, series_id, season_number, episode_number, series_years, kind): ");
    			scan.nextLine();
    			temp = scan.nextLine();
    			System.out.println();
    			
    			try {
    				selectProduction(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	
    		break;
    		
    		default:
    			System.out.println("Wrong input!");
    		break;
    	}
    }

    private static void withProductionCast()
    {
    	Scanner scan = new Scanner(System.in);
    	System.out.println("1-Add\n2-Delete\n3-Show");
    	System.out.print("What do you want to do? ");
    	int opt = scan.nextInt();
    	
    	String temp;
    	String[] parts;
    	
    	switch(opt)
    	{
    		case 1:
    			System.out.print("Enter the information respectively for the Production_Cast as shown [person_id,production_id,role_name,character_id]: ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	
    	    	parts = getInfo(temp);
    	    	
    	    	ProductionCast prodCast = new ProductionCast(parts);
    	    	try {
    				prodCast.insert(conn);
    				System.out.println();
    			} catch (SQLException e) {
    				e.printStackTrace();
    				System.out.println("ERROR!");
    			}
    	    break;
    	    
    		case 2:
    			System.out.print("Enter the deletion query for Production_Cast (cast_id, person_id, production_id, role_name, character_id): ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	System.out.println();
    	    	
	    	    try {
					delete(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	    	
    		break;
    		
    		case 3:
    			System.out.println("Enter the query you want to display about Production_Cast (cast_id, person_id, production_id, role_name, character_id): ");
    			scan.nextLine();
    			temp = scan.nextLine();
    			System.out.println();
    			
    			try {
					selectProductionCast(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	
    		break;
    		
    		default:
    			System.out.println("Wrong input!");
    		break;
    	}
    }
    
    private static void withProductionCompany()
    {
    	Scanner scan = new Scanner(System.in);
    	System.out.println("1-Add\n2-Delete\n3-Show");
    	System.out.print("What do you want to do? ");
    	int opt = scan.nextInt();
    	
    	String temp;
    	String[] parts;
    	
    	switch(opt)
    	{
    		case 1:
    			System.out.print("Enter the information respectively for the Production_Company as shown [id,production_id,company_id,kind]: ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	
    	    	parts = getInfo(temp);
    	    	
    	    	ProductionCompany prodCompany = new ProductionCompany(parts);
    	    	try {
    				prodCompany.insert(conn);
    				System.out.println();
    			} catch (SQLException e) {
    				e.printStackTrace();
    				System.out.println("ERROR!");
    			}
    	    break;
    	    
    		case 2:
    			System.out.print("Enter the deletion query for Production_Company (id, production_id, company_id, kind): ");
    			scan.nextLine();
    	    	temp = scan.nextLine();
    	    	System.out.println();
    	    	
	    	    try {
					delete(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	    	
    		break;
    		
    		case 3:
    			System.out.println("Enter the query you want to display about Production_Company (id, production_id, company_id, kind): ");
    			scan.nextLine();
    			temp = scan.nextLine();
    			System.out.println();
    			
    			try {
					selectProductionCompany(conn, temp);
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("ERROR!");
				}   	
    		break;
    		
    		default:
    			System.out.println("Wrong input!");
    		break;
    	}
    }
    
    // Deletion method
    private static void delete(Connection conn, String query) throws SQLException
    {    	
    	PreparedStatement preparedStmt = conn.prepareStatement(query);
    	preparedStmt.execute();
    }
    
    // Selection methods
    private static void selectCompany(Connection conn, String query) throws SQLException
    {
    	Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery(query);
    	    	
    	for(int i = 0; i < 100 && rs.next(); i++)
    	{
    	    int id = rs.getInt("id");
    	    String country_code = rs.getString("country_code");
    	    String name = rs.getString("name");
    	    
    	    System.out.println((i+1) + ")\t" + id + "\t" + country_code + "\t" + name + "\n");
    	}
    }
    
    private static void selectPerson(Connection conn, String query) throws SQLException
    {    	
    	Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery(query);
    	    	
    	for(int i = 0; i < 100 && rs.next(); i++)
    	{
    	    int id = rs.getInt("id");
    	    String name = rs.getString("name");
    	    String gender = rs.getString("gender");
    	    String trivia = rs.getString("trivia");
    	    String quotes = rs.getString("quotes");
    	    Date birthdate = rs.getDate("birthdate");
    	    Date deathdate = rs.getDate("deathdate");
    	    String birthname = rs.getString("birthname");
    	    String minibiography = rs.getString("minibiography");
    	    String spouse = rs.getString("spouse");
    	    float height = rs.getFloat("height");    	    
    	    
    	    System.out.println((i+1) + ")\t" + id + "\t" + name + "\t" + gender + "\t" + trivia + "\t" + quotes + "\t" + birthdate + "\t" + deathdate + "\t" + 
    	    		birthname + "\t" + minibiography + "\t" + spouse + "\t" + height + "\n");
    	}
    }
    
    private static void selectAlternateTitle(Connection conn, String query) throws SQLException
    {    	
    	Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery(query);
    	    	
    	for(int i = 0; i < 100 && rs.next(); i++)
    	{
    	    int id = rs.getInt("id");
    	    String title = rs.getString("title");
    	    int production_id = rs.getInt("production_id");
    	    
    	    System.out.println((i+1) + ")\t" + id + "\t" + title + "\t" + production_id + "\n");
    	}
    }
    
    private static void selectAlternativeName(Connection conn, String query) throws SQLException
    {    	    	
    	Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery(query);
    	    	
    	for(int i = 0; i < 100 && rs.next(); i++)
    	{
    	    int id = rs.getInt("id");
    	    String name = rs.getString("name");
    	    int person_id = rs.getInt("person_id");
    	    
    	    System.out.println((i+1) + ")\t" + id + "\t" + name + "\t" + person_id + "\n");
    	}
    }
    
    private static void selectCharacter(Connection conn, String query) throws SQLException
    {    	
    	Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery(query);
    	    	
    	for(int i = 0; i < 100 && rs.next(); i++)
    	{
    	    int id = rs.getInt("id");
    	    String name = rs.getString("name");
    	    
    	    System.out.println((i+1) + ")\t" + id + "\t" + name + "\n");
    	}
    }
    
    private static void selectProductionCast(Connection conn, String query) throws SQLException
    {    	
    	Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery(query);
    	    	
    	for(int i = 0; i < 100 && rs.next(); i++)
    	{
    		int cast_id = rs.getInt("cast_id");
    	    int person_id = rs.getInt("person_id");
    	    int production_id = rs.getInt("production_id");
    	    String role_name = rs.getString("role_name");
    	    int character_id = rs.getInt("character_id");
    	    
    	    System.out.println((i+1) + ")\t" + cast_id + "\t" + person_id + "\t" + production_id + "\t" + role_name + "\t" + character_id + "\n");
    	}
    }
    
    private static void selectProduction(Connection conn, String query) throws SQLException
    {       	
    	Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery(query);
    	    	
    	for(int i = 0; i < 100 && rs.next(); i++)
    	{
    	    int id = rs.getInt("id");
    	    String genre = rs.getString("genre");
    	    String title = rs.getString("title");
    	    int production_year = rs.getInt("production_year");
    	    int series_id = rs.getInt("series_id");
    	    int season_number = rs.getInt("season_number");
    	    int episode_number = rs.getInt("episode_number");
    	    String series_years = rs.getString("series_years");
    	    String kind = rs.getString("kind");
    	    
    	    System.out.println((i+1) + ")\t" + id + "\t" + genre + "\t" + title + "\t" + production_year + "\t" + 
    	    		series_id + "\t" + season_number + "\t" + episode_number + "\t" + series_years + "\t" + kind + "\n");
    	}
    }
    
    private static void selectProductionCompany(Connection conn, String query) throws SQLException
    {    	
    	Statement stmt = conn.createStatement();
    	ResultSet rs = stmt.executeQuery(query);
    	    	
    	for(int i = 0; i < 100 && rs.next(); i++)
    	{
    		int id = rs.getInt("id");
    	    int production_id = rs.getInt("production_id");
    	    int company_id = rs.getInt("company_id");
    	    String kind = rs.getString("kind");
    	    
    	    System.out.println((i+1) + ")\t" + id + "\t" + production_id + "\t" + company_id + "\t" + kind + "\n");
    	}
    }
    
    // Helper methods    
    private static void create_connection(String dbName,String dbUserName,String dbPassword) throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        String connectionString = "jdbc:mysql://localhost:3306/" + dbName + "?user=" + dbUserName + "&password=" + dbPassword ;
        conn = DriverManager.getConnection(connectionString);	
    }
    
    private static String[] getInfo(String info)
    {
    	ArrayList<String> temp = new ArrayList<String>();
    	StringTokenizer it = new StringTokenizer(info, ",");
    	    	
    	while(it.hasMoreTokens())
    	{
    		temp.add(it.nextToken());
    	}
    	
    	String[] result = new String[temp.size()];
    	result = temp.toArray(result);
    	
    	return result;
    }
    
    private static void database_initialize(){
    	String pass = "";
        try {
           create_connection("MovieBase", "root", pass);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private static void insertCompany()
    {
    	SVReader<Company> company;
    	
    	try{
         	company  = new SVReader<Company> ("data/COMPANY.csv" , "\t", PIECE, Company.class );
         	company.read_insert(conn);
         	System.out.println("Companies inserted");
         }
         catch (Exception e)
         {
        	 e.printStackTrace();
             System.out.println("Error.");
             return;
         }
    }
    
    private static void insertPerson()
    {
    	SVReader<Person> person;
    	
   	 	try{
        	person  = new SVReader<Person> ("data/PERSON.csv" , "\t", PIECE, Person.class );
        	person.bulk_insert(conn, "Person");
               person.bulk_insert(conn, "Person");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.out.println("Error.\n\n");
            return;
        }
   	 
   	 	System.out.println("Person inserted");
    }
    
    private static void insertAlternateTitle()
    {
        
    	SVReader<AlternateTitle> altTitle;

        try{
            altTitle = new SVReader<AlternateTitle> ("data/ALTERNATIVE_TITLE.csv", "\t", PIECE,
                    AlternateTitle.class );
            altTitle.read_insert(conn);
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            //System.out.println("Error.\n\n");
            return;
        }

        System.out.println("Alternative Title inserted");
    }
    
    private static void insertAlternativeName(){
        SVReader<AlternativeName> altName;

        try{
            altName = new SVReader<AlternativeName> ("data/ALTERNATIVE_NAME.csv", "\t", PIECE,
                    AlternativeName.class );
            altName.read_insert(conn);
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            //System.out.println("Error.\n\n");
            return;
        }

        System.out.println("Alternative Name inserted");
    }
    
    private static void insertCharacter(){
        SVReader<Character> character;

        try{
            character = new SVReader<Character> ("data/CHARACTER.csv", "\t", PIECE, Character.class );
            character.read_insert(conn);
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            //System.out.println("Error.\n\n");
            return;
        }

        System.out.println("Character inserted");
    }
    
    private static void insertProduction(){
        SVReader<Production> production;

        try{
            production = new SVReader<Production> ("data/PRODUCTION.csv", "\t", PIECE, Production.class );
            production.read_insert(conn);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            //System.out.println("Error.\n\n");
            return;
        }

        System.out.println("Production inserted");
    }
    
    private static void insertProductionCast(){
        SVReader<ProductionCast> prodcast;

        try{
            prodcast = new SVReader<ProductionCast> ("data/PRODUCTION_CAST.csv", "\t", PIECE, ProductionCast.class );
            prodcast.read_insert(conn);
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            //System.out.println("Error.\n\n");
            return;
        }

        System.out.println("Production_Cast inserted");
    }
    
    private static void insertProductionCompany()
    {
    	SVReader<ProductionCompany> prodcomp;

        try{
        	prodcomp = new SVReader<ProductionCompany> ("data/PRODUCTION_COMPANY.csv", "\t", PIECE, ProductionCompany.class );
        	prodcomp.read_insert(conn);
        }
        catch (Exception e)
        {
            //e.printStackTrace();
            //System.out.println("Error.\n\n");
            return;
        }
        
        System.out.println("Production_Company inserted");
    }
}

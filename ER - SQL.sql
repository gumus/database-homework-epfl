CREATE TABLE Person	(id INTEGER,
    name CHAR(50),
    gender CHAR(15),
    trivia CHAR(250),
    quotes CHAR(250),
    birthdate DATE,
    deathdate DATE,
    birthname CHAR(100),
    minibiography CHAR(255),
    spouse CHAR(50), 
    height FLOAT, /* It can be CHAR as well */
    PRIMARY KEY(id) );

CREATE TABLE Alternative_name	(id INTEGER,
    name CHAR(75),
    person_id INTEGER NOT NULL ,
    PRIMARY KEY(id) , 
    FOREIGN KEY(person_id) REFERENCES Person(id)
    ON DELETE CASCADE);

CREATE TABLE Production	(id INTEGER,
    genre CHAR(50),
    title CHAR(100),
    production_year DATE,
    kind CHAR(50), /* Not obligatory */
    PRIMARY KEY(id));

CREATE TABLE TV_series	(id INTEGER,
    series_years CHAR(50),
    production_id INTEGER NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(production_id) REFERENCES Production(id)
    ON DELETE CASCADE);

CREATE TABLE Episode	(series_id INTEGER NOT NULL,
    episode_number INTEGER,
    season_number INTEGER,
    production_id INTEGER NOT NULL,
    FOREIGN KEY(series_id) REFERENCES TV_series(id)
    ON DELETE CASCADE,
    FOREIGN KEY(production_id) REFERENCES Production(id)
    ON DELETE CASCADE);

CREATE TABLE Alternative_title	(id INTEGER,
    title CHAR(100),
    production_id INTEGER NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(production_id) REFERENCES Production(id)
    ON DELETE CASCADE);

CREATE TABLE Company	(id INTEGER,
    name CHAR(50),
    country_code INTEGER,
    PRIMARY KEY(id));

/* Name is changed to Characters b/c Character is a reserved word*/						 
CREATE TABLE Characters	(id INTEGER,
    name CHAR(50),
    PRIMARY KEY(id));

CREATE TABLE Production_cast	(person_id INTEGER,
    production_id INTEGER,
    character_id INTEGER,
    role_name CHAR(50),
    PRIMARY KEY(person_id, production_id),
    FOREIGN KEY(person_id) REFERENCES Person(id)
    ON DELETE CASCADE,
    FOREIGN KEY(production_id) REFERENCES Production(id)
    ON DELETE CASCADE,
    FOREIGN KEY(character_id) REFERENCES Characters(id)
    ON DELETE SET NULL);

CREATE TABLE Production_company	(id INTEGER,
    production_id INTEGER NOT NULL,
    company_id INTEGER NOT NULL,
    kind CHAR(50),
    PRIMARY KEY(id),
    FOREIGN KEY(production_id) REFERENCES Production(id)
    ON DELETE CASCADE,
    FOREIGN KEY(company_id) REFERENCES Company(id)
    ON DELETE CASCADE);

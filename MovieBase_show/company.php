<?php
/**
 * Created by PhpStorm.
 * User: orcungumus
 * Date: 25.04.15
 * Time: 09:34
 */

class company {

    protected $info = array();

    public static $vars = array('id', 'name', 'country_code');

    public function __set($name, $value = ''){
        $this->$name = $value;
    }
    public function &__get($name){
        return $this->info[$name];
    }

    function __construct(array $info = null){

        foreach($info as $key => $value)
        {
            if(in_array($key,self::$vars))
                $this->$key = $value;
            else
                $this->info[$key] = $value;
        }
    }

    public static function get_count($conn)
    {
        $countries = array();

        $query = 'SELECT country_code ,count(*) AS number_of FROM company GROUP BY country_code;';
        //echo '<div class="alert alert-info" role="alert">'.  $query .'</div>' ;

        $result = $conn->query($query);
        $numResults = $result->num_rows;

        for ($i = 0; $i < $numResults; $i++) {
            $row = $result->fetch_assoc();
            $country = array();


            $country["code"] = strtoupper($row['country_code']);
            $country["value"] = $row['number_of'] + 1;
            array_push($countries,$country);
        }
        return $countries;
    }

    public static function get_companies($conn,$wheres ,$limit, $offset){
        $companies = array();

        $query = 'SELECT * FROM company ';

        if (!empty($wheres))
            $query .=  ' WHERE ';
        foreach ($wheres as $key => $arg) {
            $query .= $key . ' = "' . $arg . '" AND ';
        }
        if (!empty($wheres))
            $query = substr($query, 0, -4);
        //Other staff

        $query .= ' LIMIT ' . $offset . ' , ' . $limit . ' ;';
        echo '<div class="alert alert-info" role="alert">'.  $query .'</div>' ;

        $result = $conn->query($query);
        $numResults = $result->num_rows;

        for ($i = 0; $i < $numResults; $i++) {
            $row = $result->fetch_assoc();
            $companies[] = new Company($row);
        }
        return $companies;
    }

}
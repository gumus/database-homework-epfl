<?php
/**
 * Created by PhpStorm.
 * User: orcungumus
 * Date: 25.04.15
 * Time: 09:54
 */
include_once ('includes/error_log.php');
include_once ('includes/config.php');
include_once ('tables.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!-- Latest compiled and minified JavaScript -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="http://code.highcharts.com/maps/highmaps.js"></script>
    <script src="http://code.highcharts.com/maps/modules/data.js"></script>
    <script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
    <script src="http://code.highcharts.com/mapdata/custom/world.js"></script>

    <script type="text/javascript" src="search.js"></script>
</head>

<body>

<?
// Check connection
if ($conn->connect_error) {
    echo '<div class="alert alert-warning" role="alert"> Connection Failed </div>';
    die("Connection failed: " . $conn->connect_error);
}
echo '<div class="alert alert-success" role="alert"> Connected successfully </div>';
?>

<div class="container">

    <div class="row">
        <div class="col-md-6">
            <h3>Company <small>table search</small></h3>
            <div id="map"></div>
            <div class="row">
                <div class="col-md-6">

                    <form class="form">
                    <div class="form-group">
                        <label for="country_code">Country Code</label>
                        <input type="text" class="form-control" id="country_code" placeholder="ch">
                    </div>
                    <div class="form-group">
                        <label for="company_name">Company Name</label>
                        <input type="text" class="form-control" id="company_name" placeholder="Vitronic">
                    </div>
                    </form>
                    <form class="form-inline">
                    <div class="form-group">
                        <label for="company_limit">Limit</label>
                        <select id="company_limit" class="form-control">
                            <option>1</option>
                            <option>10</option>
                            <option>100</option>
                            <option>1000</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="company_offset"> Offset</label>
                        <select id="company_offset" class="form-control">
                            <option>0</option>
                            <option>1000</option>
                            <option>2000</option>
                            <option>3000</option>
                        </select>
                    </div>
                    </form>
                    <br>
                    <br>
                <button type="button" onclick="search_company()" class="btn btn-default">Search</button>
                </div>
            </div>
            <br>

            <div id="company_data_area">
            <?php
                tables::show_company($conn,null ,null ,15,0);
            ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: orcungumus
 * Date: 25.04.15
 * Time: 09:19
 */


include_once('company.php');


class tables {
    public static function show_company($conn,$name, $cc, $limit, $offset)
    {
        $whereas = array();
        if(isset($cc))
            $whereas["country_code"] = $cc;
        if(isset($name))
            $whereas["name"] = $name;

        $companies = company::get_companies($conn ,$whereas, $limit, $offset);

        if (!empty($companies)) {
            echo '<table class = "table">';
            foreach ($companies as $company) {
                echo "<tr>";
                echo "<td>" . $company->id . "</td>" . "<td>" . $company->name . "<td>" . $company->country_code . "</td>";
                echo "</tr>";

            }
            echo "</table>";
        }
    }

}
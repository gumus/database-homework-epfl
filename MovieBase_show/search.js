/**
 * Created by orcungumus on 25.04.15.
 */
function search_company() {
    var name = document.getElementById("company_name").value;
    var country_code = document.getElementById("country_code").value;
    var limit = document.getElementById("company_limit").value;
    var offset = document.getElementById("company_offset").value;
// Returns successful data submission message when the entered information is stored in database.

        $.ajax({
            type: "GET",
            url: "get/company_getter.php",
            data: {name: name, country_code: country_code, limit: limit, offset: offset},
            cache: false,
            success: function(html) {
                $("#company_data_area").html(html);
            }
        });

    return false;
}

$(function () {
    console.log("Merhaba");
        $.getJSON('get/country_counter.php', function (data) {
        console.log("Merhaba");
        // Add lower case codes to the data set for inclusion in the tooltip.pointFormat

        // Initiate the chart
        $('#map').highcharts('Map', {

            title: {
                text: 'Companies per Country'
            },

            legend: {
                title: {
                    text: 'Company Count In Countries',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                    }
                }
            },

            exporting:{
                enabled: false
            },

            credits:{
                enabled: false
            },

            mapNavigation: {
                enabled: false
            },

            tooltip: {
                backgroundColor: 'none',
                borderWidth: 0,
                shadow: false,
                useHTML: true,
                padding: 0,
                pointFormat: '<span class="f32"><span class="flag {point.flag}"></span></span>'
                + ' {point.name}: <b>{point.value}</b>',
                positioner: function () {
                    return { x: 0, y: 250 };
                }
            },

            colorAxis: {
                min: 1,
                max: 104000,
                type: 'logarithmic'
            },

            series : [{
                data : data,
                mapData: Highcharts.maps['custom/world'],
                joinBy: ['iso-a2', 'code'],
                name: 'Company density',
                states: {
                    hover: {
                        color: '#BADA55'
                    }
                }
            }]
        });
    });
});
<?php
/**
 * Created by PhpStorm.
 * User: orcungumus
 * Date: 25.04.15
 * Time: 12:43
 */
include_once('../includes/error_log.php');
include_once ('../includes/config.php');
include_once('../company.php');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$contries = company::get_count($conn);

echo json_encode($contries);

?>
USE MovieBase;
SET foreign_key_checks = 0;

LOAD DATA LOCAL INFILE '/Users/Kiwi/Documents/BitBucket/ids-group/MovieBase_importdata/data/CHARACTER.csv'
    REPLACE
    INTO TABLE Characters
    (id, name);

SHOW WARNINGS;
LOAD DATA LOCAL INFILE '/Users/Kiwi/Documents/BitBucket/ids-group/MovieBase_importdata/data/COMPANY.csv'
    REPLACE
    INTO TABLE Company
    (id, country_code, name);

SHOW WARNINGS;
LOAD DATA LOCAL INFILE '/Users/Kiwi/Documents/BitBucket/ids-group/MovieBase_importdata/data/PERSON.csv'
    REPLACE
    INTO TABLE Person
    (id, name, gender, trivia, quotes, @birth, @death, birthname, minibiography, spouse,
     @height)
    SET height = 0, 
        birthdate = STR_TO_DATE(@birth, '%d %M %Y'),
        deathdate = STR_TO_DATE(@death, '%d %M %Y'),
        height = IF(@height LIKE '%\'', (CAST(TRIM(SUBSTRING_INDEX(
                                                @height, '\'', 1)) AS
                                        DECIMAL(6,3))) * 30.48,
                 IF(@height LIKE '%\'%\"', (CAST(TRIM(SUBSTRING(SUBSTRING_INDEX(
                                                             @height, '\'', -1),
                                                   1, -1)) AS
                                           DECIMAL(6,3))/12 +
                                           CAST(TRIM(SUBSTRING_INDEX(
                                                   @height, '\'', 1)) AS
                                           DECIMAL(6,3))) * 30.48,
                 IF(@height LIKE '%cm', CAST(TRIM(SUBSTRING_INDEX(
                                                @height, 'cm', 1)) AS
                                           DECIMAL(6,3)),
                 NULL)));
SHOW WARNINGS;
LOAD DATA LOCAL INFILE '/Users/Kiwi/Documents/BitBucket/ids-group/MovieBase_importdata/data/PRODUCTION.csv'
    REPLACE
    INTO TABLE Production
    (id, title, @prod_date, series_id, season_number, episode_number, series_years, kind, genre)
    SET production_year = CONVERT(@prod_date, UNSIGNED);

SHOW WARNINGS;
LOAD DATA LOCAL INFILE '/Users/Kiwi/Documents/BitBucket/ids-group/MovieBase_importdata/data/PRODUCTION_COMPANY.csv'
    REPLACE
    INTO TABLE Production_company
    (id, company_id, production_id, kind);

SHOW WARNINGS;
LOAD DATA LOCAL INFILE '/Users/Kiwi/Documents/BitBucket/ids-group/MovieBase_importdata/data/PRODUCTION_CAST.csv'
    REPLACE
    INTO TABLE Production_cast
    (production_id, person_id, character_id, role_name);
SHOW WARNINGS;

LOAD DATA LOCAL INFILE '/Users/Kiwi/Documents/BitBucket/ids-group/MovieBase_importdata/data/ALTERNATIVE_TITLE.csv'
    REPLACE
    INTO TABLE Alternative_title
    (id, production_id, title);
SHOW WARNINGS;

LOAD DATA LOCAL INFILE '/Users/Kiwi/Documents/BitBucket/ids-group/MovieBase_importdata/data/ALTERNATIVE_NAME.csv'
    REPLACE
    INTO TABLE Alternative_name
    (id, person_id , name );
SHOW WARNINGS;
